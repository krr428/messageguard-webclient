/**
 * Gulp file loads tasks from ./gulp/tasks.
 */
"use strict";

var gulp        = require('gulp'),
    minimist    = require('minimist'),
    requireDir  = require('require-dir'),
    taskListing = require('gulp-task-listing');

// Tasks to run when no task specified.
var defaultTasks = ['extension'];

// Parse command-line options.
var argv = minimist(process.argv.slice(2), {
  boolean: ['debug', 'watch'],
  alias:   {d: 'debug', w: 'watch'},
  default: {debug: true, watch: false}
});

// Determine whether we are building the extension.
var tasks = argv._.length ? argv._ : defaultTasks;

// Remove the '_' param before assigning to the global flags.
delete argv._;
global.flags = argv;

global.buildingExtension = tasks.some(function (val) { return /^extension(:.*)?$/.test(val); });
global.buildingBookmarklet = tasks.some(function (val) { return /^server(:.*)?$/.test(val); });
if (global.buildingExtension && global.buildingBookmarklet) {
  throw new Error("Can't build both the extension and bookmarklet at the same time.");
}

// Setup gulp tasks.
gulp.task('default', defaultTasks);
gulp.task('list', taskListing);
requireDir('./gulp/tasks');
