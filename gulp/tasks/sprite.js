/**
 * File for generating sprites and their associated Sass files.
 */

var assign = require('lodash.assign'),
    config = require('../config'),
    del    = require('del'),
    gulp   = require('gulp'),
    gwatch = require('../resources/gulp-watch'),
    sprity = require('sprity');

// Find out if we are already watching everything.
var watchingSprite, watchingSpriteFrontend;

// Generate the sprites.
gwatch.task('sprite', ['sprite:frontend'], function () {
  if (!watchingSprite && global.flags.watch) {
    watchingSprite = true;
    gulp.watch(config.sprite.srcFiles, ['sprite:nodeps']);
  }

  return sprity.src(config.sprite.sprityOptions)
      .pipe(gulp.dest(config.sprite.buildPath));
});

/**
 * Clean up the sprites.
 */
gulp.task('sprite:clean', function () {
  return del(config.sprite.buildPath);
});

// Move the gulp sprite class over. Common functionality for all sprites.
gwatch.task('sprite:frontend', function () {
  if (!watchingSpriteFrontend && global.flags.watch) {
    watchingSpriteFrontend = true;
    gulp.watch(config.sprite.frontendSrcFiles, ['sprite:frontend:nodeps']);
  }

  return sprity.src(assign({}, config.sprite.sprityOptions, {
        base64:   true,
        src:      config.sprite.frontendSrcFiles,
        template: './gulp/resources/frontend.hbs',
        style:    '_sprite-frontend.scss'
      }))
      .pipe(gulp.dest(config.sprite.buildPath));
});
