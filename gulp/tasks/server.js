/**
 * Server tasks.
 */
var config       = require('../config'),
    del          = require('del'),
    insert       = require('gulp-insert'),
    gulp         = require('gulp'),
    gwatch       = require('../resources/gulp-watch'),
    merge        = require('merge-stream'),
    sftp         = require('gulp-sftp'),
    paramReplace = require('../resources/param-replace'),
    rename       = require('gulp-rename'),
    uglify       = require('gulp-uglify');

// Upload files to the server.
gwatch.task('server', ['server:files'], function () {
  return gwatch.src(config.server.buildFiles, null, ['server:nodeps'])
      .pipe(sftp(config.server.sftpOptions));
});

// Clean up any temporary files.
gulp.task('server:clean', ['js:clean', 'sass:clean'], function () {
  return del(config.server.buildPath);
});

// Get the server files.
gwatch.task('server:files', ['js', 'js:bookmarklet', 'sass'], function () {
  return merge(
      // Handle the javascript.
      gwatch.src(config.js.buildFiles, null, ['server:files:nodeps'])
          .pipe(gulp.dest(config.server.buildPath + 'js')),
      gwatch.src(config.js.buildBookmarkletFiles, null, ['server:files:nodeps'])
          .pipe(gulp.dest(config.server.buildPath + 'js')),

      // Add HTML files to the root directory.
      gwatch.src(config.html.srcFiles, null, ['server:files:nodeps'])
          .pipe(gulp.dest(config.server.buildPath + 'html')),

      // Include the css files.
      gwatch.src(config.sass.buildFiles, null, ['server:files:nodeps'])
          .pipe(gulp.dest(config.server.buildPath)),

      // Include the sprites.
      gwatch.src(config.sprite.buildFiles, null, ['server:files:nodeps'])
          .pipe(gulp.dest(config.server.buildPath + 'img')),

      // Include remaining resources.
      gwatch.src(config.resources.srcFiles, null, ['server:files:nodeps'])
          .pipe(gulp.dest(config.server.buildPath)),

      // Add the bookmarklet
      gwatch.src(config.server.bookmarkletFile, null, ['server:files:nodeps'])
          .pipe(paramReplace())
          .pipe(uglify())
          .pipe(insert.wrap('javascript:(function(){', '})();'))
          .pipe(rename('bookmarklet.url'))
          .pipe(gulp.dest(config.server.buildPath)),

      // Add any website files. This is a site that users can visit. Really just used to help debugging though.
      gwatch.src(config.server.siteFiles, null, ['server:files:nodeps'])
          .pipe(gulp.dest(config.server.buildPath))
  );
});