/**
 * Front end tasks.
 */
"use strict";

var brfs   = require('brfs'),
    bundle = require('../resources/bundle'),
    config = require('../config'),
    del    = require('del'),
    gulp   = require('gulp'),
    gwatch = require('../resources/gulp-watch'),
    jshint = require('gulp-jshint'),
    merge  = require('merge-stream');

// Build all of the bundles, except for browser/extension specific bundles.
gulp.task('js', ['js:options', 'js:overlays']);

// Clean up the build directory.
gulp.task('js:clean', ['js:doc:clean'], function () {
  return del(config.js.buildPath);
});

// Bundle the extension front end.
gulp.task('js:extension', ['sass:frontend', 'sass:messageGuard'], function () {
  return bundle(config.js.srcPath + 'frontend/extension.js', 'extension.js',
                {browserify: {transform: brfs}, skipSourcemap: true});
});

// Bundle the bookmarklet front end.
gulp.task('js:bookmarklet', ['sass:frontend', 'sass:messageGuard'], function () {
  return bundle(config.js.srcPath + 'frontend/bookmarklet.js', 'bookmarklet.js',
                {browserify: {transform: brfs}});
});

// Lint the javascript files.
gulp.task('js:lint', function () {
  return gwatch.src(config.js.srcFiles, null, ['js:lint'])
      .pipe(jshint(config.js.lintPreferences))
      .pipe(jshint.reporter('jshint-stylish'));
});

// Clean up documentation.
gulp.task('js:doc:clean', function () {
  del(config.js.docPath);
});

// Options page.
gulp.task('js:options', function () {
  return merge(
      bundle(config.js.srcPath + 'key-management/main/index.js', 'key-management/index.js'),
      bundle(config.js.srcPath + 'key-management/main/initialize.js', 'key-management/initialize.js'),
      bundle(config.js.srcPath + '/options.js', 'options.js')
  );
});

// Bundle the front end.
gulp.task('js:overlays', function () {
  return merge(
      bundle(config.js.srcPath + 'overlays/generic-read-main.js', 'overlays/generic-read.js'),
      bundle(config.js.srcPath + 'overlays/generic-compose-main.js', 'overlays/generic-compose.js'),

      bundle(config.js.srcPath + 'overlays/custom/gmail/compose.js', 'overlays/custom/gmail/compose.js'),
      bundle(config.js.srcPath + 'overlays/custom/gmail/read.js', 'overlays/custom/gmail/read.js'),

      bundle(config.js.srcPath + 'overlays/key-management/default.js', 'overlays/key-management/default.js'),
      bundle(config.js.srcPath + 'overlays/key-management/standalone.js', 'overlays/key-management/standalone.js'),

      bundle(config.js.srcPath + 'key-management/communicator/receiver/worker.js', 'key-manager-worker.js')
  );
});
