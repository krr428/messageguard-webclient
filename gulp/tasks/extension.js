/**
 * Extension tasks.
 */

var config = require('../config'),
    del    = require('del'),
    format = require('util').format,
    gulp   = require('gulp'),
    gwatch = require('../resources/gulp-watch'),
    merge  = require('merge-stream'),
    shell  = require('gulp-shell');

// Alias 'extension' to the default build platform
gulp.task('extension', [format('extension:%s', config.extension.defaultPlatform)]);

// Clean up any temporary files.
gulp.task('extension:clean', ['js:clean', 'sass:clean'], function () {
  return del([config.extension.stagingPath, config.extension.buildPath]);
});

// Setup tasks for each browser.
Object.keys(config.extension.buildTargets).forEach(function (platform) {
  var task       = format('extension:%s', platform),
      watchTask  = format('extension:%s:watch', platform),
      nodepsTask = format('extension:%s:nodeps', platform),
      targetCmd  = config.extension.buildTargets[platform];

  gwatch.task(task, [watchTask], shell.task(targetCmd));

  gulp.task(watchTask, ['extension:files'], function () {
    return gwatch.src(config.extension.stagingFiles, null, [nodepsTask]);
  });
});

// Get the options file.
gwatch.task('extension:files', ['js', 'js:extension', 'sass', 'extension:setup'], function () {
  return merge(
      // Handle the JavaScript.
      gwatch.src(config.js.buildFiles, null, ['extension:files:nodeps'])
          .pipe(gulp.dest(config.extension.stagingResourcePath + 'js')),
      gwatch.src(config.js.buildExtensionFiles, null, ['extension:files:nodeps'])
          .pipe(gulp.dest(config.extension.stagingResourcePath + 'js')),

      gwatch.src(config.html.srcFiles, null, ['extension:files:nodeps'])
          .pipe(gulp.dest(config.extension.stagingResourcePath + 'html')),

      // Include the css files.
      gwatch.src(config.sass.buildFiles, null, ['extension:files:nodeps'])
          .pipe(gulp.dest(config.extension.stagingResourcePath)),

      // Include the sprites.
      gwatch.src(config.sprite.buildFiles, null, ['extension:files:nodeps'])
          .pipe(gulp.dest(config.extension.stagingResourcePath + 'img')),

      // Include remaining resources.
      gwatch.src(config.resources.srcFiles, null, ['extension:files:nodeps'])
          .pipe(gulp.dest(config.extension.stagingResourcePath))
  );
});

// Move the extension template files to where they need to be.
gulp.task('extension:setup', function () {
  return gwatch.src(config.extension.templateFiles, null, ['extension:setup'])
      .pipe(gulp.dest(config.extension.stagingPath));
});
