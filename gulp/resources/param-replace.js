/**
 * Replace params (e.g., @Server) in the javascript files.
 *
 * This class is based on gulp-replace.
 */
"use strict";

var assign         = require('lodash.assign'),
    config         = require('../config'),
    istextorbinary = require('istextorbinary'),
    rs             = require('replacestream'),
    Transform      = require('readable-stream/transform');

/**
 * Parameters to be replaced.
 * @type {Object}
 */
var paramReplacements = (function (params) {
  var replacements = {};

  replacements[params.labels.server] = global.buildingBookmarklet ? config.server.baseUrl : '';
  replacements[params.labels.isExtension] = global.buildingExtension ? 'true' : 'false';

  assign(replacements, params.terms);

  return replacements;
})(config.replacements);

/**
 * Run the replacements.
 * @param {Object} data Data to be modified.
 * @param {Function} func Function used to modify data.
 */
var runReplacements = function (data, func) {
  Object.keys(paramReplacements).forEach(function (property) {
    data = func(data, '@' + property, paramReplacements[property]);
  });
  return data;
};

/**
 * Replace the paramaters in the javascript files.
 */
module.exports = function (options) {
  return new Transform({
    objectMode: true,
    transform:  function (file, enc, callback) {
      if (file.isNull()) {
        return callback(null, file);
      }

      function doReplace() {
        if (file.isStream()) {
          file.contents = runReplacements(file.contents, function (data, search, replacement) {
            return data.pipe(rs(search, replacement));
          });
          return callback(null, file);
        }

        if (file.isBuffer()) {
          file.contents = new Buffer(runReplacements(String(file.contents), function (data, search, replacement) {
            if (search instanceof RegExp) {
              return data.replace(search, replacement);
            }
            else {
              var chunks = data.split(search);

              if (typeof replacement === 'function') {
                replacement = replacement(search);
              }

              return chunks.join(replacement);
            }
          }));

          return callback(null, file);
        }

        callback(null, file);
      }

      if (options && options.skipBinary) {
        istextorbinary.isText(file.path, file.contents, function (err, result) {
          if (err) {
            return callback(err, file);
          }

          if (!result) {
            callback(null, file);
          } else {
            doReplace();
          }
        });

        return;
      }

      doReplace();
    }
  });
};