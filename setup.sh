#!/bin/bash

sudo npm install -g gulp bower
npm install
bower install

if [ ! -f ./gulp/.ftppass ]; then
  cp ./gulp/.ftppass.example ./gulp/.ftppass
fi

if [ ! -f ./gulp/user-config.js ]; then
  cp ./gulp/user-config.js.example ./gulp/user-config.js
fi


