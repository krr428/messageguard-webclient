call npm install -g gulp bower

call npm install
call bower install

IF NOT EXIST ./gulp/.ftppass (
  copy .\gulp\.ftppass.example .\gulp\.ftppass
)

IF NOT EXIST ./gulp/user-config.js (
  copy .\gulp\user-config.js.example .\gulp\user-config.js
)

