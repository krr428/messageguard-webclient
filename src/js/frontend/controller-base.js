/**
 * Abstract base for controllers. Allows subclasses to only replace read or compose scanning as needed. Also handles
 * selecting elements given a set of selectors for compose. Will look for all read messages that match any package
 * and the given selector.
 * <br /><br />
 * Must set the compose selector if the sublass will use this base class's compose scanner.
 *
 * @module
 * @requires frontend/observer
 * @requires common/selectors
 */
"use strict";

var Observer  = require('./observer'),
    selectors = require('../common/selectors'),
    urls      = require('../common/urls'),
    UUID      = require('../common/uuid');

/**
 * Create a controller with the given selectors.
 * @param {string} [composeSelector] Selector to search for compose elements.
 * @param {string} [composeContentEditableSelector] Selector of elements who we care about if contentEditable.
 * @param {string} [readSelector] Selector to search for read elements.
 * @constructor
 * @alias module:frontend/controller-base
 */
function ControllerBase(composeSelector, composeContentEditableSelector, readSelector) {

  /**
   * Set of active overlays managers.
   * @type {!Array.<module:frontend/overlay-manager-base>}
   * @protected
   */
  this.overlayManagers = [];

  /**
   * Overlay managers to setup.
   * @type {Object}
   * @prop {!Array.<module:frontend/overlay-manager-base>} setup Overlay managers to setup.
   * @prop {!Array.<module:frontend/overlay-manager-base>} setup Overlay managers to move.
   * @prop {!Array.<module:frontend/overlay-manager-base>} setup Overlay managers to remove.
   * @prop {!boolean} Whether the update has been scheduled.
   * @protected
   */
  this.managersToUpdate = {
    setup:           [],
    moved:           [],
    cleanup:         [],
    updateScheduled: false
  };

  /**
   * Constructors for the overlay managers.
   * @type {Object}
   * @prop {!Function} read Read overlay manager constructor.
   * @prop {!Function} compose Compose overlay manager constructor.
   * @protected
   */
  this.constructors = {
    read:    null,
    compose: null
  };

  /**
   * Whether or not the controller has started scanning the page.
   * @type {!boolean}
   * @protected
   */
  this.started = false;

  /**
   * Selector string that matches compose elements.
   * @type {string}
   * @protected
   */
  this.composeSelector = composeSelector;

  /**
   * Selector for elements that have [contentEditable] in {@link #composeSelector}.
   * @type {string}
   * @protected
   */
  this.composeContentEditableSelector = composeContentEditableSelector;

  /**
   * Selector string that matches read elements.
   * @type {string}
   * @protected
   */
  this.readSelector = readSelector;

  /**
   * An observer for the document. Used to watch changes.
   * @type {module:frontend/observer}
   * @protected
   */
  this.observer = new Observer(document.documentElement);

  /**
   * A UUID for the window, for intra-window storage communication.
   * @type {string}
   * @protected
   */
  this.windowUUID = UUID();

  /**
   * A reference to the key manager iframe. Placed on a page during start()
   * @type {HTMLElement}
   * @protected
   */
  this.keyManagerIframe = null;
}

/**
 * Add an overlay for the given item.
 * @param {!Element} item Item that potentially needs to be overlayed.
 * @param {!Function} Constructor Constructor for the appropriate overlay manager.
 * @protected
 */
ControllerBase.prototype.addOverlayManager = function (item, Constructor) {
  // Find if there is an overlay manager for this item already.
  for (var i = 0, len = this.overlayManagers.length; i < len; i++) {
    if (this.overlayManagers[i].isManagerFor(item)) {
      this.enqueueUpdate(this.overlayManagers[i], 'moved');
      return;
    }
  }

  // Remove any decendant overlays.
  this.removeOverlayManager(item, true);

  // Add the overlay.
  var overlayManager = new Constructor(item, this.windowUUID);
  this.overlayManagers.push(overlayManager);
  this.enqueueUpdate(overlayManager, 'setup');
};

/**
 * Move the overlay manager that manages the given item.
 * @param {!Element} item Item that if owned by another overlay is moved.
 * @protected
 */
ControllerBase.prototype.moveOverlayManager = function (item) {
  // Find if there is an overlay manager for this item already.
  for (var i = 0, len = this.overlayManagers.length; i < len; i++) {
    if (this.overlayManagers[i].isSelf(item)) {
      this.enqueueUpdate(this.overlayManagers[i], 'moved');
      return;
    }
  }
};

/**
 * Remove the overlay manager that manages the given item.
 * @param {!Element} item Item that if owned by an overlay is removed.
 * @param {boolean} [ignoreSelf] Whether the overlay should only be deleted if it is a decendant.
 * @protected
 */
ControllerBase.prototype.removeOverlayManager = function (item, ignoreSelf) {
  // Find if there is an overlay manager for this item already.
  for (var i = 0, len = this.overlayManagers.length; i < len; i++) {
    var isSelf       = this.overlayManagers[i].isSelf(item),
        isDescendant = this.overlayManagers[i].isDecendant(item);

    var shouldRemove = ignoreSelf ? isDescendant : isSelf || isDescendant;

    if (shouldRemove) {
      this.enqueueUpdate(this.overlayManagers[i], 'cleanup');
      this.overlayManagers.splice(i, 1);
      return;
    }
  }
};

/**
 * Queues an update operation.
 * @param {!module:frontend/overlay-manager-base} manager Manager to update.
 * @param {string} queueName Update queue to put the manager.
 * @protected
 */
ControllerBase.prototype.enqueueUpdate = function (manager, queueName) {
  // If we are cleaning something up we don't need to update or complete it.
  if (queueName === 'cleanup') {
    var index = this.managersToUpdate.setup.indexOf(manager);
    if (index !== -1) this.managersToUpdate.setup.splice(index, 1);
    index = this.managersToUpdate.moved.indexOf(manager);
    if (index !== 1) this.managersToUpdate.moved.splice(index, 1);
  } else {
    if (this.managersToUpdate.cleanup.indexOf(manager) !== -1) return;
  }

  // Add the manager to the queue unless it is already there.
  var queue = this.managersToUpdate[queueName];
  if (queue.indexOf(manager) !== -1) return;
  queue.push(manager);

  // Schedule the update.
  if (!this.managersToUpdate.updateScheduled) {
    this.managersToUpdate.updateScheduled = true;
    window.setTimeout(this.updateManagers.bind(this), 0);
  }
};

/**
 * Update the managers in the queue.
 * @protected
 */
ControllerBase.prototype.updateManagers = function () {
  var i, len;
  this.managersToUpdate.updateScheduled = false;

  // Stop the observer so we don't get events we are generating.
  this.observer.stop();

  for (i = 0, len = this.managersToUpdate.cleanup.length; i < len; i++) this.managersToUpdate.cleanup[i].cleanup();
  for (i = 0, len = this.managersToUpdate.setup.length; i < len; i++) this.managersToUpdate.setup[i].setup();
  for (i = 0, len = this.managersToUpdate.moved.length; i < len; i++) this.managersToUpdate.moved[i].onMoved();

  // Restart the observer.
  this.observer.start();

  this.managersToUpdate.cleanup = [];
  this.managersToUpdate.setup = [];
  this.managersToUpdate.moved = [];
};

/**
 * Scan the given node for read packages and overlay them.
 * @param {!Element} node Node to scan.
 */
ControllerBase.prototype.scanForReadPackage = function (node) {
  // Ignore anything in the head.
  if (selectors.isAncestor(node, document.head)) return;

  var foundParents = [];
  var walker = document.createTreeWalker(node, NodeFilter.SHOW_TEXT,
                                         function (node) {
                                           // Check if we have already found a parent element for this node.
                                           for (var i = 0, len = foundParents; i < len; i++) {
                                             if (selectors.isAncestor(node, foundParents[i])) return false;
                                           }
                                           return node.textContent.indexOf('|mg|') !== -1;
                                         });

  var matchingNode;
  while ((matchingNode = walker.nextNode()) !== null) {
    var parent = matchingNode.parentNode;

    // TODO: Use the packagers to do testing instead.
    //if (/\|mg\|[A-Za-z0-9\+\/]*={0,2}\|/.test(parent.textContent)) {
    foundParents.push(parent);

    this.addOverlayManager(parent, this.constructors.read);
    //}
  }
};

/**
 * Start the controller. If both skipReadOverlays and skipComposeOverlays are true, ControllerBase will handle watching
 * for moved and removed overlays.
 * @param {boolean} [skipReadOverlays] If true will skip creating read overlays.
 * @param {boolean} [skipComposeOverlays] If true will skip creating compose overlays.
 */
ControllerBase.prototype.start = function (skipReadOverlays, skipComposeOverlays) {
  if (this.started) return; // Run once.
  this.started = true;

  // Scan for added composed elements.
  if (!skipComposeOverlays) {
    var observerParams = {
      added:        true,
      subtree:      true,
      elementsOnly: true
    };
    this.observer.observe(observerParams, this.composeSelector, function (item) {
      this.addOverlayManager(item, this.constructors.compose);
    }.bind(this));

    // Scan for elements whose contentEditable changed.
    // TODO: We ignore removed contentEditable attribute, but this should be evaluated for correctness.
    // TODO: add rules to ignore search bars?
    this.observer.observe({
                            attributes:      true,
                            attributeFilter: ['contenteditable'],
                            subtree:         true
                          }, this.composeContentEditableSelector, function (item) {
      if (item.contentEditable === 'true' || item.contentEditable === 'plaintext-only') {
        this.addOverlayManager(item, this.constructors.compose);
      }
    }.bind(this));
  }

  // Scan for added read elements.
  if (!skipReadOverlays) {
    this.observer.observe({added: true, modified: true, subtree: true}, this.readSelector, function (item) {
      this.scanForReadPackage(item);
    }.bind(this));
  }

  // Scan for moved overlays.
  this.observer.observe({moved: true, subtree: true}, function (item) {
    this.moveOverlayManager(item);
  }.bind(this));

  // Scan for removed overlays.
  this.observer.observe({removed: true, subtree: true}, function (item, record, mutationType) {
    if (!selectors.isAncestor(item, document.body)) {
      this.removeOverlayManager(item);
    }
  }.bind(this));

  // Search for the initial set of overlays.
  if (document.body) {
    var i, len;

    if (!skipComposeOverlays) {
      var composeElements = document.querySelectorAll(this.composeSelector);
      for (i = 0, len = composeElements.length; i < len; i++) {
        this.addOverlayManager(composeElements[i], this.constructors.compose);
      }
    }

    if (!skipReadOverlays) {
      if (this.readSelector) {
        var readElements = document.querySelectorAll(this.readSelector);
        for (i = 0, len = readElements.length; i < len; i++) {
          this.scanForReadPackage(readElements[i]);
        }
      } else {
        this.scanForReadPackage(document.body);
      }
    }
  }

  this.addKeyManagerIframe();

  // Start the observer.
  this.observer.start();
};

ControllerBase.prototype.addKeyManagerIframe = function () {
  if (this.keyManagerIframe) {
    return;
  }

  this.keyManagerIframe = document.createElement('iframe');

  this.keyManagerIframe.src =
      urls.getOverlayUrl('key-management/default-key-manager.html', {window_uuid: this.windowUUID});
  this.keyManagerIframe.style.display = 'none';
  this.keyManagerIframe.width = '0';
  this.keyManagerIframe.height = '0';
  this.keyManagerIframe.tabIndex = '-1';
  this.keyManagerIframe.title = 'empty'; // For screen readers that might see this.

  // Try every half-second to add the key manager iframe, until document.body is available.
  var tryIntervalID = null,
      tryAdd        = (function () {

        if (document.body) {
          window.clearInterval(tryIntervalID);
          document.body.appendChild(this.keyManagerIframe);
        }
      }).bind(this);

  tryIntervalID = window.setInterval(tryAdd, 0.5 * 1000);
};

module.exports = ControllerBase;
