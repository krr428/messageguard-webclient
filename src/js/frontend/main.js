/**
 * If this module is required it will startup MessageGuard's frontend.
 * @module
 * @requires frontend/generic/controller
 */
"use strict";

var controller;

if (/^mail\.google\.com$/i.test(location.hostname)) {
  controller = new (require('./custom/gmail/controller'))();
} else {
  controller = new (require('./generic/controller'))();
}

if (controller) {
  controller.start();
}
