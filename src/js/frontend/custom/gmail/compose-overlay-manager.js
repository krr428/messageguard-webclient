/**
 * Generic compose overlay manager.
 * @module
 * @requires frontend/overlayer
 * @requires frontend/overlay-manager-base
 * @requires common/message-type
 */
//"use strict";

var communicatorMixin  = require('../../../common/communicator-mixin'),
    jQuery             = require('jquery'),
    MessageType        = require('../../../common/message-type'),
    MouseEvents        = require('./mouse-events'),
    Overlayer          = require('../../overlayer'),
    OverlayManagerBase = require('../../overlay-manager-base'),
    PackageWrapper     = require('./package-wrapper'),
    PageScanner        = require('./page-scanner'),
    UUID               = require('../../../common/uuid');

require('./jquery-observe');

/**
 * Create a compose overlay manager for the given item.
 * @param {!Element} item Item that is being overlayed.
 * @constructor
 * @extends module:frontend/overlay-manager-base
 * @alias module:frontend/generic/compose-overlay-manager
 */
function ComposeOverlayManager(item, windowUUID) {
  this.item = item;
  this.windowUUID = windowUUID;
  this.overlayer = new Overlayer(this.item.querySelector('table.iN'),
      jQuery(this.item).closest('.h7.Jux0I.j23lnd').length ? false : true); // On non-reply compose, clip to the window.
  this._overlay = null;
  this.overlayURL = 'custom/gmail/gmail-compose.html';
  communicatorMixin.call(this);

  this.element = item;
  this.emailRegex =
      /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/i;
  this.heightOffset = 0;
  this.instanceUUID = UUID();
}

ComposeOverlayManager.prototype = Object.create(OverlayManagerBase.prototype);
ComposeOverlayManager.prototype.constructor = ComposeOverlayManager;

ComposeOverlayManager.prototype.getElement = function () {
  return jQuery('table.iN', this.element);
};

/**
 * @method getClippingElement
 * @return {Element} The parent element.
 */
ComposeOverlayManager.prototype.getClippingElement = function () {
  return this.element;
};

/**
 * @method getRecipients
 * @return Array.<string> The recipients.
 */
ComposeOverlayManager.prototype.getRecipients = function () {
  var emailRegex = this.emailRegex;
  var recipients = jQuery('input[name="to"],input[name="cc"],input[name="bcc"]', this.element).filter(function () {
    return emailRegex.test(this.value);
  });
  return jQuery.map(recipients, function (item) { return item.value.match(emailRegex)[0]; });
};

/**
 * @method getSubject
 * @return {string} The value of the 'subject' field.
 */
ComposeOverlayManager.prototype.getSubject = function () {
  return jQuery("input[name='subjectbox']", this.element)[0].value;
};

/**
 * @method isSubjectFocused
 * @return {boolean} Whether the subject field is currently focused.
 */
ComposeOverlayManager.prototype.isSubjectFocused = function () {
  return jQuery("input[name='subjectbox']", this.element).is(':focus');
};


/**
 * @method setSubject
 * @param {string} to The value of the 'subject' field.
 */
ComposeOverlayManager.prototype.setSubject = function (subject) {
  jQuery("input[name='subjectbox']", this.element)[0].value = subject.trim();
};

// Get's the preamble.
ComposeOverlayManager.prototype.getPreamble = function () {
  return jQuery('.composePreamble > textarea', this.element).val();
};

// Set's the preamble.
ComposeOverlayManager.prototype.setPreamble = function (preamble) {
  if (preamble && preamble.trim) {
    jQuery('.composePreamble > textarea', this.element).val(preamble.trim());
  }
};

/**
 * @method getBodyElement
 * @return {Element} the body element for this compose area
 */
ComposeOverlayManager.prototype.getBodyElement = function () {
  return jQuery('div[contentEditable]', this.element);
};

/**
 * @method getBody
 * @return {string} The inner text of the body field.
 */
ComposeOverlayManager.prototype.getBody = function () {
  return this.getBodyElement().html();
};

/**
 * Sets the body of the message.
 * @method setBody
 * @param {string} The inner text of the body field.
 */
ComposeOverlayManager.prototype.setBody = function (body) {
  this.getBodyElement().html(body);
};

/**
 * Checks whether this compose area is a reply to an encrypted message.
 * @method isEncryptedReply
 * @return {Boolean} Whether this is a reply to an encrypted message.
 */
ComposeOverlayManager.prototype.isEncryptedReply = function () {
  var parentElement = jQuery(this.element).closest('.h7.Jux0I.j23lnd');
  if (!parentElement) {
    return false;
  }
  return jQuery('div[name="encryptedEmail"]', parentElement).length != 0;
};

/**
 * Setup the area to support enabling encryption.
 */
ComposeOverlayManager.prototype.setupArea = function (clickHandler) {
  var lockBar = document.createElement('div');
  lockBar.className = "messageGuardBar";

  var lockEncryption = document.createElement('div');
  lockEncryption.className = "messageGuardLockDiv";
  lockEncryption.style = 'display: none;';
  lockEncryption.innerText = 'Encrypted';

  // Span with info about wether the element is currently locked or not.
  var lockBarInfo = document.createElement('span');
  lockBarInfo.className = "messageGuardInfo";
  jQuery(lockBarInfo).data('encrypted-message', "This message is being encrypted");
  jQuery(lockBarInfo).data('unencrypted-message', "This message is not being encrypted");
  lockBarInfo.innerText = jQuery(lockBarInfo).data('unencrypted-message');

  var lockButton = document.createElement('div');
  lockButton.className = "messageGuardLockButton";
  lockButton.innerText = 'Turn on Encryption';
  lockButton.addEventListener('click', clickHandler, false);

  jQuery(lockBar).append(lockEncryption, lockBarInfo, lockButton);
  jQuery(".fX.aXjCH", this.element).before(lockBar);

  // Change the send button.
  jQuery('div.aoO[role="button"]', this.element).html('Send unencrypted');
  jQuery('div.aA4', this.element).css('left', '140px');
  jQuery('.Uz', this.element).css({margin: '0'});

  // Fired when this compose area is moved into the maximized window.
  var self = this;
  if (self.isEncryptedReply() || PackageWrapper.hasWrappedPackage(self.getBody())) {
    clickHandler();
  } else {
    // Give the draft some time to load.
    window.setTimeout(function () {
      if (self.isEncryptedReply() || PackageWrapper.hasWrappedPackage(self.getBody())) {
        clickHandler();
      }
    }, 1000);
  }
};

//#endregion

//#region Button Manipulators

/**
 * Gets the send button.
 * @method getSendButton
 * @return {Element} The send button (or gmail's approximation of a button)
 */
ComposeOverlayManager.prototype.getSendButton = function () {
  //With new compose window no longer need to go up to parents
  return jQuery("div:contains('Send')[role='button']", this.element)[0];
};

/**
 * Clicks the send button in this form.
 * @method clickSendButton
 */
ComposeOverlayManager.prototype.clickSendButton = function () {
  this.ensureRichTextMode();
  MouseEvents.simulateMouseClick(this.getSendButton());
};

/**
 * Clicks the save draft button in this form.
 * @method clickSendButton
 */
ComposeOverlayManager.prototype.clickSaveDraftButton = function () {
  if (!this.getBodyElement().length) {
    return;
  }

  this.ensureRichTextMode();

  // Emulate changing the document to enable the save draft button, and then click it.
  var event = document.createEvent('KeyboardEvent');
  event.initKeyboardEvent('keydown', true, true, null, 'Enter', 0, null, null, null);
  this.getBodyElement()[0].dispatchEvent(event);
};

/**
 * Simulates a mouse click to expand the reply text.
 */
ComposeOverlayManager.prototype.expandReply = function () {
  var expander = jQuery('.ajT', this.element);
  if (expander.length > 0) {
    MouseEvents.simulateMouseClick(expander[0]);
  }
};

/**
 * Discards the draft.
 */
ComposeOverlayManager.prototype.discardDraft = function () {
  var expander = jQuery('.og.T-I-J3', this.element);
  if (expander.length > 0) {
    MouseEvents.simulateMouseClick(expander[0]);
  }
};

//#endregion

//#region Element Manipulation

/**
 * Ensure that the message is in rich text mode.
 */
ComposeOverlayManager.prototype.ensureRichTextMode = function () {
  // Ensure that we are in rich-text mode.
  var parentMenu = jQuery(this.element).closest(".gB,[role=dialog]").first();
  var plainTextSelector = parentMenu.find('[role=menuitemcheckbox]:contains("Plain text mode")');

  // If the menu has never been shown, we need to generate it.
  if (plainTextSelector.length == 0) {
    MouseEvents.simulateMouseClick(jQuery('.Xv[role=button]', parentMenu)[0]);
    MouseEvents.simulateMouseClick(jQuery('.Xv[role=button]', parentMenu)[0]);
    plainTextSelector = parentMenu.find('[role=menuitemcheckbox]:contains("Plain text mode")');
  }

  this.originalCheckedState = plainTextSelector.hasClass('J-Ks-KO');
  if (this.originalCheckedState) {
    MouseEvents.simulateMouseClick(plainTextSelector[0]);
  }
};

/**
 * Prepare the area to use encrypted email.
 */
ComposeOverlayManager.prototype.prepareArea = function () {
  var self = this;

  // Placeholder update methods.
  var subjectBox = jQuery('input[name="subjectbox"]', this.element);
  this.subjectBlur = function () {
    subjectBox.prop('placeholder', 'Subject - The subject will not be encrypted.');
  };
  var toBox = jQuery('textarea[name="to"]', this.element);
  this.toBlur = function () {
    toBox.prop('placeholder', 'This message will be encrypted for these recipients.');
  };
  var recipientBox = jQuery('.aoD.hl .oL.aDm', this.element);
  this.recipientBlur = function () {
    recipientBox.text('Recipients - This message will be encrypted for these recipients.');
  };

  // Store original placeholder values
  this.originalSubjectPlaceholder = subjectBox.prop('placeholder');
  this.originalToPlaceHolder = toBox.prop('placeholder');
  this.originalRecipientPlaceHolder = recipientBox.text();

  // Update the current values and add listeners.
  subjectBox.on('blur', this.subjectBlur).on('focus', this.subjectBlur);
  toBox.on('blur', this.toBlur).on('focus', this.toBlur);
  recipientBox.on('blur', this.recipientBlur).on('focus', this.recipientBlur);

  this.subjectBlur();
  this.toBlur();
  this.recipientBlur();

  // Add a preamble if not an encrypted reply.
  if (!this.isEncryptedReply() && jQuery('div.composePreamble', this.element).length == 0) {
    jQuery(".aoD.az6:last", this.element).after([
                                                  '<div class="aoD az6 composePreamble">',
                                                  '<textarea class="aoT art" ' +
                                                  'placeholder="Greeting (Optional) - Add an unencrypted personal greeting at the start of your email. This lets recipients know that your message is genuine and not spam."' +
                                                  'spellcheck="true" tabindex="1" />',
                                                  '</div>'
                                                ].join(''));
  }
  jQuery('div.composePreamble', this.element).show();

  // Hide the encryption-not supported icons from Gmail.
  jQuery('span[role="button"].bcU', this.element).hide();

  // Change message box text.
  var messageGuardEncrypted = jQuery(".messageGuardLockDiv", this.element);
  messageGuardEncrypted.show();
  var messageGuardInfo = jQuery(".messageGuardInfo", this.element);
  messageGuardInfo.text(messageGuardInfo.data("encrypted-message"));
  var messageGuardLockButton = jQuery(".messageGuardLockButton", this.element);
  messageGuardLockButton.hide();

  // Set a class to indicate that we have started encrypting the item.
  jQuery('.iN', this.element).addClass('messageGuardComposeOverlayed');

  this.getElement().css('min-height', '375px');
  this.hideUnsupportedElements();
};

/**
 * Revert the area to non-encrypted mod.
 */
ComposeOverlayManager.prototype.revertArea = function () {
  // Reset place holder values.
  jQuery('input[name="subjectbox"]', this.element).prop('placeholder', this.originalSubjectPlaceholder);
  jQuery('textarea[name="to"]', this.element).prop('placeholder', this.originalToPlaceHolder);
  jQuery('.aoD.hl .oL.aDm', this.element).text(this.originalRecipientPlaceHolder);

  // Remove handlers
  jQuery('input[name="subjectbox"]', this.element).off('blur', this.subjectBlur).off('focus', this.subjectBlur);
  jQuery('textarea[name="to"]', this.element).off('blur', this.toBlur).off('focus', this.toBlur);
  jQuery('.aoD.hl .oL.aDm', this.element).off('blur', this.recipientBlur).off('focus', this.recipientBlur);

  // Hide preamble div.
  jQuery('div.composePreamble', this.element).hide();

  // Hide the encryption-not supported icons from Gmail.
  jQuery('span[role="button"].bcU', this.element).show();

  // Change message box text.
  var messageGuardEncrypted = jQuery(".messageGuardLockDiv", this.element);
  messageGuardEncrypted.hide();
  var messageGuardInfo = jQuery(".messageGuardInfo", this.element);
  messageGuardInfo.text(messageGuardInfo.data("unencrypted-message"));
  var messageGuardLockButton = jQuery(".messageGuardLockButton", this.element);
  messageGuardLockButton.show();

  // Unset a class to indicate that we have stoped encrypting the item.
  jQuery('.iN', this.element).removeClass('messageGuardComposeOverlayed');

  this.showUnsupportedElements();
};

/**
 * Get's elements that should not be shown while the overlay is being shown.
 * @method getUnsupportedElements
 */
ComposeOverlayManager.prototype.getUnsupportedElements = function () {
  return jQuery('table.iN tr:nth-child(2)', this.element);
};

/**
 * Removes the fixed positioning class.
 */
ComposeOverlayManager.prototype.removeFixedPositioningClass = function () {
  var item = jQuery(this);
  if (item.hasClass('aDi')) {
    item.disconnect({attributes: true, attributeFilter: ['class']}, arguments.callee);
    item.data('originalClass', this.className);
    item.removeClass('aDi');
    item.observe({attributes: true, attributeFilter: ['class']}, arguments.callee);
  }
};

/**
 * Hide's elements that should not be shown while the overlay is being shown.
 * @method hideUnsupportedElements
 */
ComposeOverlayManager.prototype.hideUnsupportedElements = function () {
  var self = this;
  this.getUnsupportedElements().hide();

  var elements = jQuery('.aDj', this.element);
  elements.observe({attributes: true, attributeFilter: ['class']}, this.removeFixedPositioningClass);
  elements.each(function () {
    jQuery(this).data('originalClass', this.className);
    self.removeFixedPositioningClass();
  });

  jQuery(".messageGuardLockButton", this.element).css("visibility", "hidden");
};

/**
 * Shows elements that were hidden when the overlay was not being shown.
 * @method showUnsupportedElements
 */
ComposeOverlayManager.prototype.showUnsupportedElements = function () {
  this.getUnsupportedElements().show();

  var elements = jQuery('.aDj', this.element);
  elements.disconnect({attributes: true, attributeFilter: ['class']}, this.removeFixedPositioningClass);
  var self = this;
  elements.each(function () {
    this.className = jQuery(this).data('originalClass');
  });

  jQuery(".messageGuardLockButton", this.element).css("visibility", "visible");
};

/**
 * Composes an email to a contact prompting them to install MessageGuard.
 * @method composeInstallPromptEmail
 * @param {string} The email address to compose this message for.
 */
ComposeOverlayManager.prototype.composeInstallPromptEmail = function (emailAddress) {
  var KEYSERVER_HOSTNAME = 'messageguard.io',
      keyServerLinkHref  = 'https://' + KEYSERVER_HOSTNAME,
      keyServerLink      = '<a href="' + keyServerLinkHref + '">MessageGuard.io</a>',
      mailtoSubject      = 'Trying to send you an encrypted message',
      mailtoBody         = '<div>' +
                           'Hey,' +
                           '<br><br>' +
                           'I want to send you an encrypted message using MessageGuard, but I need you to install it first.' +
                           '<br><br>' +
                           'Here\'s what you\'ll need to do:' +
                           '<ol>' +
                           '<li>Go to ' + keyServerLink + ' and sign up for an account.</li>' +
                           '<li>Download and install MessageGuard.</li>' +
                           '<li>Let me know when you\'ve set it up, and I\'ll send you my encrypted message.</li>' +
                           '</ol>' +
                           'Hope that helps!' +
                           '</div>';

  var onMessageOpened = function (composeContainer) {
    var toField      = jQuery(composeContainer).find('textarea[name="to"]'),
        subjectField = jQuery(composeContainer).find('input[name="subjectbox"]'),
        bodyArea     = jQuery(composeContainer).find('[contenteditable]:last'); // :last not necessary at the moment,
                                                                                // just a precaution.

    if (typeof emailAddress == 'object') {
      toField.text(emailAddress.join(', '));
      setTimeout(function () {
        toField.blur();
        subjectField.focus();
        MouseEvents.simulateMouseClick(subjectField[0]);
      }, 200);
    } else {
      toField.text(emailAddress);
    }

    subjectField.val(mailtoSubject);
    bodyArea.html(mailtoBody + bodyArea.html());
  }

  var clickComposeAndWait = function () {
    return new Promise(function (resolve) {

      var composeWindowSelector = '.nH.Hd',
          initialComposeWindows = jQuery(composeWindowSelector);

      var lookForNewComposeWindow = function () {
        var currentComposeWindows = jQuery(composeWindowSelector),
            newComposeWindows     = currentComposeWindows.not(initialComposeWindows);

        if (newComposeWindows.length == 0) {
          // We haven't seen a new one yet, wait another quarter-second.
          setTimeout(lookForNewComposeWindow, 250);
          return;
        }

        resolve(newComposeWindows.first());
      };

      // Open a new compose window.
      var sendButton = jQuery('[role="button"]:contains(COMPOSE)');
      MouseEvents.simulateMouseClick(sendButton);

      // Wait for the new window to open.
      lookForNewComposeWindow();
    });
  };

  var maximizedComposeBackground = jQuery('.aSs');

  var shrinkCurrentWindow;

  if (maximizedComposeBackground.css('visibility') == 'visible') {

    // We'll need to minimize the current compose window, and then after that we can
    // click "Compose".

    shrinkCurrentWindow = new Promise(function (resolve) {

      // Like lookForNewComposeWindow, this function repeatedly checks until the
      // compose background is hidden. before resolving this promise.
      var waitForMinimized = function () {
        if (maximizedComposeBackground.css('visibility') == 'visible') {
          setTimeout(waitForMinimized, 100);
          return;
        }

        resolve();
      };

      // Click to minimize the compose window, and wait for it to get minimized.
      MouseEvents.simulateMouseClick(maximizedComposeBackground);
      waitForMinimized();
    });
  } else {
    shrinkCurrentWindow = Promise.resolve();
  }

  shrinkCurrentWindow.then(function () {
    return clickComposeAndWait().then(onMessageOpened);
  });
};


/**
 * Setup and begin operating.
 */
ComposeOverlayManager.prototype.setup = function () {
  OverlayManagerBase.prototype.setup.call(this);

  var addComposeOverlayCallback = function () {
    this.prepareArea();
    this.placeOverlay();
  }.bind(this);

  this.setupArea(addComposeOverlayCallback.bind(this));
};

/**
 * Updates the compose area with information from the package.
 * @method updateComposeArea
 * @param {kiwiPackage} Package The package to update the compose area from.
 */
ComposeOverlayManager.prototype.updateComposeArea = function (package, isDraftEvent) {
  var packageWrapper = PackageWrapper.createWriteWrapper(this.getSubject(), this.getPreamble(), package);

  // Always wrap the subject if we're not saving a draft.
  // If we are, check to see if there's any text there, and if we're not focused on it.
  if (!isDraftEvent || (this.getSubject().trim() && !this.isSubjectFocused())) {
    this.setSubject(packageWrapper.wrappedSubject);
  }

  this.setBody(packageWrapper.wrappedBody);
};

/**
 * Called when the overlay is ready to communicate. Thus, messagehandlers should
 * be defined and registered here.
 */
ComposeOverlayManager.prototype.onOverlayReady = function () {
  var self = this;

  this.registerMessageHandler(MessageType.MESSAGE_ENCRYPTED, false, function (data) {
    this.updateComposeArea(data);
    this.removeOverlay();
    this.revertArea();
    jQuery('div.aoO[role="button"]', this.element).html('Send');
    this.clickSendButton();
  });

  this.registerMessageHandler(MessageType.SAVE_DRAFT, false, function (data) {
    this.updateComposeArea(data, true);
    this.clickSaveDraftButton();
  });

  this.registerMessageHandler(MessageType.DISCARD_DRAFT, false, function () {
    self.discardDraft();
  });

  this.registerMessageHandler(MessageType.CANCEL, false, function (data) {
    if (PackageWrapper.isWrappedSubject(this.getSubject())) {
      this.setSubject(PackageWrapper.unwrapSubject(this.getSubject()));
    }
    this.setBody(data);

    this.removeOverlay();
    this.revertArea();
  });

  this.registerMessageHandler(MessageType.GET_RECIPIENTS, false, function () {
    this.postMessage(MessageType.GET_RECIPIENTS, this.getRecipients());
  });

  this.registerMessageHandler(MessageType.GET_CONTENTS, false, function () {
    var info = {
      userId:      PageScanner.getEmailAddress(),
      allowCancel: !this.isEncryptedReply()
    };

    // If the message being composed has a wrapped message, replace the entire wrapped section with the message body.
    this.expandReply();
    var packageWrapper = PackageWrapper.createReadWrapper(this.getSubject(), this.getBody());
    if (packageWrapper != null) {
      this.setPreamble(packageWrapper.preamble);
      info.subject = packageWrapper.subject;
      info.body = PackageWrapper.getTemplate(this.getBody(), "[MessageGuard Package]"); // Replace the package
      // document with the actual
      // package.
      info.templateItem = "[MessageGuard Package]";
      info.package = packageWrapper.package;
    }
    else {
      info.subject = this.getSubject();
      info.body = this.getBody();
      info.templateItem = null;
      info.package = null;
    }

    this.postMessage(MessageType.SEND_CONTENTS, info);
  });

  this.registerMessageHandler(MessageType.COMPOSE_INSTALL_PROMPT, false, function (data) {
    this.composeInstallPromptEmail(data);
  });

  this.focusHandler = function () {
    window.setTimeout(function () {
      if (this._overlay) {
        this._overlay.contentWindow.focus();
        this.postMessage(MessageType.FOCUS_CHANGED);
      }
    }.bind(this), 0);
  };

  // Setup a handler to redirect tabbing into the compose area.
  this.getBodyElement().on('focus', this.focusHandler.bind(this));

  jQuery('.composePreamble > textarea', this.element).keydown(function (e) {
    if ((e.keyCode || e.which) == 9 && !e.shiftKey) {
      e.preventDefault();
      this.focusHandler();
    }
  }.bind(this));

  // Shift+tab pressed within overlay; take focus back to the preamble.
  // Currently does not work.
  this.registerMessageHandler(MessageType.FOCUS_CHANGED, false, function (data) {
    setTimeout(function () {
      jQuery('.composePreamble > textarea', this.element).focus();
    }.bind(this), 10);
  });

  this.postMessage(MessageType.OVERLAY_READY);
};

ComposeOverlayManager.prototype.cleanup = function () {
  OverlayManagerBase.prototype.cleanup.call(this);
};

module.exports = ComposeOverlayManager;
