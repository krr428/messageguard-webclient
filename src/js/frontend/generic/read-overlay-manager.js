/**
 * Generic read overlay manager.
 * @module
 * @requires frontend/overlay-manager-base
 * @requires common/message-type
 */
"use strict";

var OverlayManagerBase = require('../overlay-manager-base'),
    MessageType        = require('../../common/message-type');

//noinspection JSClosureCompilerSyntax
/**
 * Create a read overlay manager for the given item.
 * @param {!Element} item Item that is being overlayed.
 * @constructor
 * @extends module:frontend/overlay-manager-base
 * @alias module:frontend/generic/read-overlay-manager
 */
function ReadOverlayManager(item, windowUUID) {
  OverlayManagerBase.call(this, item, windowUUID);
  this.overlayURL = 'generic-read.html';
}

ReadOverlayManager.prototype = new OverlayManagerBase();
ReadOverlayManager.prototype.constructor = ReadOverlayManager;

/**
 * Setup and begin operating.
 */
ReadOverlayManager.prototype.setup = function () {
  OverlayManagerBase.prototype.setup.call(this);
  this.placeOverlay();
};

/**
 * Called when the overlay is ready to communicate.
 */
ReadOverlayManager.prototype.onOverlayReady = function () {
  this.postMessage(MessageType.SEND_CONTENTS, this.item.textContent);
};

module.exports = ReadOverlayManager;
