/**
 * Main key management file.
 */

var $            = require('../../common/jquery-bootstrap'),
    UUID         = require('../../common/uuid'),
    urls         = require('../../common/urls'),
    KeyManager   = require('../key-manager'),
    Errors       = require('../../common/errors'),
    EventEmitter = require('../../common/storage/storage-objects').eventEmitter;

var options = urls.getUrlOptions();

/**
 * Emit an event, and wait for a reply. On reply, close the window.
 * This is the only way we could get popup windows like this to close themselves.
 *
 * @param destination - name of event to post data to.
 * @param data        - data to post for event.
 */
function sendAndWait(destination, data) {
  if (!destination) {
    return;
  }

  data = data || {};
  data.replyUUID = UUID();

  EventEmitter.emit(destination, data);

  // Wait for the reply. On recepit, close the window.
  EventEmitter.once(data.replyUUID, function (e) {
    if (e.details.command == 'close') {
      window.close();
    }
  });
}


// Decide what to do based on the window's URL options.

if ('prompt_master_password' in options) {

  // Render a master-password modal prompt.

  var replyUUID   = options.reply_uuid,
      errName     = options.original_err_name,
      originalErr = errName in Errors ? new Errors[errName]() : null;

  KeyManager.promptMasterPassword(originalErr).then(function () {
    sendAndWait(replyUUID);
  });
} else if ('prompt_create_key' in options) {

  // Render a key-creation modal prompt.

  // scheme: name of scheme to create key for.
  // fingerprints: array of fingerprints; the created key must match at least one of them, if any are given.
  // promptType: string to determine what type of prompt this is, if it's for encryption or decryption.
  // fullKeyOptions: if true, the key color and name fields are rendered.
  // replyUUID: Event to fire when key is created.

  var scheme         = options.scheme || Object.keys(KeyManager.uiManagers)[0],
      fingerprints   = JSON.parse(options.fingerprints || '[]'),
      promptType     = options.prompt_type,
      fullKeyOptions = options.fullKeyOptions == 'true',
      replyUUID      = options.reply_uuid;

  KeyManager.createKey(scheme, promptType, fullKeyOptions, fingerprints).then(function (key) {
    sendAndWait(replyUUID, {
      attributes: key.attributes.serialize()
    });
  });
} else {

  // Render the main options UI.

  KeyManager.ensureKeyStoragePasswordPresent().catch(function (e) {
    if (!(e instanceof Errors.MasterPasswordNotFoundError)) {
      alert(e.message);
      return;
    }

    return KeyManager.promptMasterPassword(e);
  }).then(function () {
    return KeyManager.setupUI();
  });
}
