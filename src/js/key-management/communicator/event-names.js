/**
 * Enumerates all labels for any tasks that the key manager might need to perform for the overlay.
 */

var names = {
  ENCRYPT_DATA:                   'encrypt_data',
  DECRYPT_DATA:                   'decrypt_data',
  ENCRYPT_DRAFT:                  'encrypt_draft',
  DECRYPT_DRAFT:                  'decrypt_draft',
  ENCRYPT_KEY:                    'encrypt_key',
  DECRYPT_KEY:                    'decrypt_key',
  CAN_ENCRYPT:                    'can_encrypt',
  GET_KEY_INFO:                   'get_key_info',
  ENSURE_MASTER_PASSWORD_PRESENT: 'ensure_master_password_present',
  INITIALIZE_KEYS:                'initialize_keys',
  HEARTBEAT:                      'heartbeat',
  KEYS_UPDATED:                   'keys_updated',
  MASTER_PASSWORD_INITIALIZED:    'master_password_initialized'
};

module.exports = {
  /**
   * Calls a handler function for every name and label in the 'names' object.
   * @param handler - function to call for every key-name pair.
   */
  mapNames: function (handler) {
    Object.keys(names).forEach(function (key) {
      handler(key, names[key]);
    });
  }
};

/**
 * Populate the exports object with the names and labels of the events.
 */
module.exports.mapNames(function (key, name) {
  module.exports[key] = name;
});
