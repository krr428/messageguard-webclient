/**
 * Functions for forwarding tasks from the main key management overlay to the
 * key manager worker thread.
 *
 * 1) For every event, a handler is registered to TaskRequestHandler.
 *
 * 2) This handler generates a unique replyUUID, passes it to the worker thread,
 *    and returns a promise that waits for a local eventEmitter instance to emit
 *    on that UUID, resolving to the emitted data.
 *
 * 3) On any message received from the worker thread, if it is in response to a
 *    task, the local eventEmitter instance will emit on the returned replyUUID.
 *    The promise waiting on that UUID will then resolve.
 *
 * In addition, there is code that interfaces with native storage objects, that are
 * not available in the worker thread. If the worker thread requests access to
 * localStorage, for instance, a message is passed to this main thread, which passes
 * the request data to the proper native storage object and replies with the retrieved
 * data.
 *
 * This code also supports local storage events. The implementation details are found
 * within common/storage/storage-objects/native-objects.js
 */

var LocalEventEmitter = new (require('events'))();

var EventNames           = require('../event-names'),
    Errors               = require('../../../common/errors'),
    UUID                 = require('../../../common/uuid'),
    urls                 = require('../../../common/urls'),
    NativeStorageObjects = require('../../../common/storage/storage-objects/native-objects'),
    TaskRequestHandler   = require('./task-request-handler');

var worker;

/**
 * Posts a message to the worker. This is currently only used for storage data requests;
 * either in response to a data request from the worker thread, or in response to a storage
 * event from the main thread.
 *
 * @param replyUUID   - if present, represents the replyUUID given from the worker thread, so the
 *                      response data can be properly routed in the worker thread.
 * @param messageType - string representing the type of message being sent.
 *                      Either 'storage_event' or 'storage_response'.
 * @param message     - object representing the storage data.
 */
function postWorkerMessage(replyUUID, messageType, message) {
  var data = {
    replyUUID: replyUUID,
    msgType:   messageType,
    msg:       message
  };

  worker.postMessage(data);
}

/**
 * Posts a task o the worker. Used to forward tasks from the main thread to the worker thread.
 *
 * @param replyUUID   - ID given to worker thread, to be passed back with the task response, so
 *                      the response can be properly routed.
 * @param taskName    - name of task to be performed.
 * @param taskDetails - details associated with the task.
 */
function postWorkerTask(replyUUID, taskName, taskDetails) {
  var data = {
    replyUUID: replyUUID,
    taskName:  taskName,
    task:      taskDetails
  };

  worker.postMessage(data);
}

module.exports = {

  /**
   * Initializes the worker thread, and message listeners.
   */
  init: function () {

    worker = new Worker(urls.getScriptUrl('key-manager-worker.js'));

    /**
     * Processes worker messages. Messages can either be responses to task requests, or requests for
     * storage object data.
     *
     * @param e - event object
     */
    worker.onmessage = function (e) {
      // replyUUID is either the ID we generated in the first place and passed to the worker in postWorkerTask,
      // or it is an ID generated on the worker thread and we should pass it back with the requested storage object
      // data.
      if (!e.data.replyUUID) {
        return;
      }

      switch (e.data.msgType) {
        case 'storage_request':

          /*
          We're responding to a storage object request. We'll generate two functions:

          requestCB: if this is a storage event request, events may need to be transmitted back to the worker
                     thread in the future. This function is passed in to allow the native storage event module
                     to create a listener that will do that.

          responseCB: this function dispatches a response to the worker thread, with the results of the storage
                      request.

          Once the native storage object has handled the request, either a data response or an error response is
          issued.
           */

          var requestCB  = postWorkerMessage.bind(null, null, 'storage_event'),
              responseCB = postWorkerMessage.bind(null, e.data.replyUUID, 'storage_response');

          NativeStorageObjects.handleWorkerRequest(e.data.msg, requestCB).then(function (result) {
            responseCB({result: result});
          }).catch(function (error) {
            responseCB({error: error});
          });
          break;
        default:

          /*
          We're handling a task response. Emit on the given replyUUID, so the promise that is waiting on this
          task can be resolved.
           */
          LocalEventEmitter.emit(e.data.replyUUID, e.data.msg);
      }
    };

    /**
     * Returns a handler function that takes in task details and forwards them to the worker thread.
     *
     * @param taskName - name of task for the returned handler function. Passed to postWorkerTask.
     * @returns {Function} - handler function for forwarding tasks to the worker thread.
     */
    var eventForwarder = function (taskName) {
      return function (taskDetails) {
        var replyUUID = UUID();

        return new Promise(function (resolve, reject) {
          // Set up the reply receiver before posting the task to the worker.
          LocalEventEmitter.once(replyUUID, function (msg) {
            if (msg.error) {
              reject(Errors.parse(msg.error));
            } else {
              resolve(msg.result);
            }
          });

          postWorkerTask(replyUUID, taskName, taskDetails);
        });
      };
    };

    // For every event name, create a forwarder function and register it as the handler
    // for that event.
    EventNames.mapNames(function (eventLabel, eventName) {
      TaskRequestHandler.registerHandler(eventName, eventForwarder(eventName));
    });
  }
};
