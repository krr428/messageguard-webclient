/*
* Shared symmetric key that encrypts via AES
* Only lives in session storage.
*/

var PasswordBase = require('./base-passwords');

var SCHEME_NAME = 'Password';

function EphemeralPasswordsSystem() {
  PasswordBase.system.apply(this, arguments);

  this.attributes.scheme = SCHEME_NAME;
  this.attributes.storage = 'session';
}

EphemeralPasswordsSystem.prototype = Object.create(PasswordBase.system.prototype);

var scheme = new PasswordBase.scheme(SCHEME_NAME, EphemeralPasswordsSystem);
scheme.usesEncryptedStorage = false;

module.exports = scheme;
