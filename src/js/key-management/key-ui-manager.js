/**
 * Key UI Manager, responsible for managing the UI for a given scheme.
 */

var util = require('util');

var $          = require('../common/jquery-bootstrap'),
    Errors     = require('../common/errors'),
    KeyStorage = require('./key-storage');

// Only require it if we successfully got jQuery (as in, if we're not in a worker thread).
if (typeof $ === 'function') {
  // Must come after requiring jquery-bootstrap, since internally
  // this package requires jquery. We must make sure that bootstrap
  // initializes before this.
  require('spectrum-colorpicker');
}

/**
 * Constructor. If jQuery is available, set up the key-color button.
 *
 * @param keyScheme
 * @constructor
 */
function KeyUIManager(keyScheme) {
  this.scheme = keyScheme;

  if (typeof $ === 'function') {
    $(this._setupKeyIcon.bind(this));
  }
}

/**
 * Returns the page's key-system-modal element.
 *
 * @returns {jQuery|HTMLElement}
 * @private
 */
KeyUIManager.prototype._modal = function () {
  return $('#key-system-modal');
};

/**
 * Sets up the UI for this scheme. Adds a row in the UI for every existing key system.
 */
KeyUIManager.prototype.setupUI = function () {
  var self = this;
  self.ui = $('<div class="key-system-rows"></div>');

  return KeyStorage.getByScheme(self.scheme.name).then(function (keySystems) {
    keySystems.map(function (keySystem) {
      self.ui.append(self._makeKeyRow(keySystem));
    });

    return self.ui;
  });
};

/**
 * Sets up the key color picker, using the spectrum plugin.
 * @private
 */
KeyUIManager.prototype._setupKeyIcon = function () {
  var modal = this._modal();

  // Only set it up once.
  if (modal.data('has-been-set-up')) {
    return;
  } else {
    modal.data('has-been-set-up', true);
  }

  var updateColor = function (color) {
    modal.find('.key-icon').css('color', typeof color == 'string' ? color : color.toHexString());
  };

  var spectrumOptions = {
    showButtons:            false,
    showPalette:            true,
    showPaletteOnly:        true,
    hideAfterPaletteSelect: true,
    palette:                [
      ["#000", "#444", "#666", "#999", "#ccc", "#eee", "#f3f3f3", "#fff"],
      ["#f00", "#f90", "#ff0", "#0f0", "#0ff", "#00f", "#90f", "#f0f"],
      ["#f4cccc", "#fce5cd", "#fff2cc", "#d9ead3", "#d0e0e3", "#cfe2f3", "#d9d2e9", "#ead1dc"],
      ["#ea9999", "#f9cb9c", "#ffe599", "#b6d7a8", "#a2c4c9", "#9fc5e8", "#b4a7d6", "#d5a6bd"],
      ["#e06666", "#f6b26b", "#ffd966", "#93c47d", "#76a5af", "#6fa8dc", "#8e7cc3", "#c27ba0"],
      ["#c00", "#e69138", "#f1c232", "#6aa84f", "#45818e", "#3d85c6", "#674ea7", "#a64d79"],
      ["#900", "#b45f06", "#bf9000", "#38761d", "#134f5c", "#0b5394", "#351c75", "#741b47"],
      ["#600", "#783f04", "#7f6000", "#274e13", "#0c343d", "#073763", "#20124d", "#4c1130"]
    ],
    move:                   updateColor,
    change:                 updateColor
  };

  modal.find('.key-icon').spectrum(spectrumOptions);
};

/**
 * Add a newly-created key to the rows of key systems.
 *
 * @param key - new key system to add to the page's UI.
 * @returns {jQuery|HTMLElement} - row that was added.
 * @private
 */
KeyUIManager.prototype._makeKeyRow = function (key) {
  var self = this;

  var row = $('<div class="row">'
              + '<div class="col-md-1"><span class="key-color fa fa-key"></span></div>'
              + '<div class="col-md-9"><span class="key-name"></span></div>'
              + '<div class="col-md-1"><span class="edit-btn fa fa-pencil-square-o"></span></div>'
              + '<div class="col-md-1"><span class="remove-btn fa fa-times"></span></div>'
              + '</div>');

  row.find('.key-color').css('color', key.attributes.color);
  row.find('.key-name').text(key.attributes.name);

  row.find('.edit-btn').click(function () {
    self._showKeyEditor(key).then(function () {
      return KeyStorage.update(key);
    }).then(function () {
      row.replaceWith(self._makeKeyRow(key));
      self._modal().modal('hide');
    });
  });

  row.find('.remove-btn').click(function () {
    KeyStorage.remove(key).then(row.remove.bind(row));
  });

  return row;
};

/**
 * Populates the key editor modal for the given key, and displays it.
 *
 * @param key - key to show editor for.
 * @private
 */
KeyUIManager.prototype._showKeyEditor = function (key) {
  var self = this;

  self._configureModal(key);

  return new Promise(function (resolve) {
    self.scheme.getUI(key, self._setModalBodySaveable.bind(self), 'standard').then(function (keyElement) {
      self._setEditorMode('standard', true);
      var modal = self._modal();

      modal.find('.editor-contents').empty().append(keyElement);
      modal.find('.save-btn').off('click.save').on('click.save', resolve);
      modal.modal('show');
      $(keyElement).find(':input').first().focus();
    });
  }).then(function () {
    key.attributes.color = self._modal().find('.key-icon').css('color');
  });
};

/**
 * Based on the key system's attributes, configure the modal elements to represent the key system's state.
 *
 * @param key - key system to configure modal for.
 * @private
 */
KeyUIManager.prototype._configureModal = function (key) {
  var self = this;
  var modal = self._modal();

  modal.find('input.system-name, .create-btn').toggle(typeof key == 'undefined');
  modal.find('h4.system-name, .save-btn').toggle(typeof key != 'undefined');

  if (key) {
    modal.find('.key-icon').css('color', key.attributes.color);
    modal.find('h4.system-name').text(key.attributes.name);
  } else {
    modal.find('.key-icon').css('color', 'black');
    modal.find('input.system-name').val('').off('keyup.system-name').on('keyup.system-name', function () {
      //self._setModalHeaderSaveable($(this).val() != '')
    });
  }

  //this._setModalHeaderSaveable(typeof key != 'undefined'); // If the key is undefined, start off as non-saveable.
  this._setModalHeaderSaveable(true);
  this._setModalBodySaveable(false);
};

/**
 * @param isSaveable - whether the header fields are set such that the key is saveable.
 * @private
 */
KeyUIManager.prototype._setModalHeaderSaveable = function (isSaveable) {
  this._modalHeaderSaveable = isSaveable;
  this._updateModalSaveable();
};

/**
 * Passed to scheme.getUI() to notify us when the key is saveable.
 *
 * @param isSaveable - whether the body fields are set such that the key is saveable.
 * @private
 */
KeyUIManager.prototype._setModalBodySaveable = function (isSaveable) {
  this._modalBodySaveable = isSaveable;
  this._updateModalSaveable();
};

/**
 * Updates the modal's saveable state.
 * @private
 */
KeyUIManager.prototype._updateModalSaveable = function () {
  var isSaveable = this._modalHeaderSaveable && this._modalBodySaveable;
  this._modal().find('.save-btn, .create-btn').prop('disabled', !isSaveable);
};

KeyUIManager.prototype._clickSaveButton = function () {
  this._modal().find('.create-btn, .save-btn').filter(':visible:not(:disabled)').click();
};

/**
 * Updates key modal UI fields based on the type of prompt this is.
 *
 * @param promptType - either 'standard', 'decrypt', or 'encrypt'
 * @param fullKeyOptions - boolean, whether to show the header fields.
 * @private
 */
KeyUIManager.prototype._setEditorMode = function (promptType, fullKeyOptions) {
  var modal = this._modal();

  //var shouldHideHeader = promptType != 'standard' && !fullKeyOptions;
  var shouldHideHeader = false;
  var createBtnText = promptType == 'standard' ? 'Create' : 'OK';
  var isStandard = promptType == 'standard';

  if (shouldHideHeader) {
    modal.find('.modal-header input.system-name').val(this.scheme + ' key').trigger('change.system-name');
  }

  modal.find('.modal-header').toggle(!shouldHideHeader);
  modal.find('.modal-footer .create-btn').text(createBtnText);

  modal.find('.modal-footer .cancel-btn').toggle(isStandard);
  modal.find('button.close').toggle(isStandard);
  modal.modal(
      {
        backdrop: isStandard ? true : 'static',
        keyboard: isStandard
      });
};

/**
 * Adds a new key system. Initializes the modal, shows it, and stores the created key.
 *
 * @param promptType - string representing the type of prompt - 'standard', 'encrypt', 'decrypt'.
 * @param fullKeyOptions - boolean representing whether to show the key color and name fields.
 * @param fingerprintsToMatch - array of fingerprints that this key should match.
 */
KeyUIManager.prototype.addKeySystem = function (promptType, fullKeyOptions, fingerprintsToMatch) {
  var self = this;

  self._configureModal();

  var callbacks = {
    isSaveable: this._setModalBodySaveable.bind(this),
    saveClick:  this._clickSaveButton.bind(this)
  };

  return self.scheme.getUI(null, callbacks, promptType).then(
      function (keyElement) {
        self._setEditorMode(promptType, fullKeyOptions);
        var modal = self._modal();
        modal.find('.editor-contents').empty().append(keyElement);

        return new Promise(function (resolve) {

          /**
           * Function called upon successful key creation. Sets the name and color attributes, hides the modal,
           * and stores the key before resolving.
           *
           * @param key - newly created key.
           */
          var keyCreated = function (key) {
            modal.find('.modal-footer .alert').hide();

            key.attributes.name = modal.find('.system-name').val() || self.scheme.name + ' Key';
            key.attributes.color = modal.find('.key-icon').css('color');

            if (promptType && promptType != 'standard' && !fullKeyOptions) {
              key.attributes.color = 'black';

              if (key.attributes.id) {
                key.attributes.name = util.format('%s key (%s)', key.attributes.scheme, key.attributes.id.value);
              } else {
                key.attributes.name = util.format('%s key', key.attributes.scheme);
              }
            }

            if (self.ui) {
              self.ui.append(self._makeKeyRow(key));
            }

            KeyStorage.add(key).then(function () {
              modal.modal('hide');
              resolve(key);
            });
          };

          modal.find('.create-btn').off('click.create').on('click.create', function (e) {
            self.scheme.create(keyElement, fingerprintsToMatch).then(keyCreated).catch(function (e) {
              if (!(e instanceof Errors.KeyCreateFailedError)) {
                return reject(e);
              }

              modal.find('.modal-footer .alert .alert-text').text(
                  'Whoops! This key will not be able to decrypt the message.');
              modal.find('.modal-footer .alert').show();
            });
          });

          modal.modal('show');
        });
      });
};

KeyUIManager.prototype.handleError = function (keyError) {
  // TODO - handle properly
  console.log(keyError);
};

module.exports = KeyUIManager;
