/**
 * URLs used by the front end. Currently just the overlays. For the extension, gets extension-local URLs.
 * @module
 */
/* global kango:false */

var Errors = require('./errors');

/**
 * Handles getting the overlay urls.
 */
module.exports = {

  /**
   * MessageGuard is running inside an extension.
   */
  isExtension: '@IsExtension' === 'true',

  /**
   * Gets the origin of the urls. Will be initialized below.
   */
  origin: null,

  /**
   * Gets the base of the urls. Will be initialized below.
   */
  base: null,

  /**
   * Get the specified resource.
   * @param {!string} overlay Path relative to the resource folder for the resource to get.
   * @param {Object} [options] Options to append to the url.
   * @returns {!string} URL to the resource.
   */
  getResourceUrl: function (resource, options) {
    if (typeof window == 'undefined') {
      throw new Errors.MGError('Cannot use URL functions; window not defined.');
    }

    options = options || {};

    var url;
    if (this.isExtension) {
      url = this.base + '/res/' + resource;
    } else {
      url = this.base + '/' + resource;
    }

    Object.keys(options).forEach(function (key, i) {
      if (i === 0) {
        url += '#?';
      } else {
        url += '&';
      }

      url += window.encodeURIComponent(key) + '=' +
             window.encodeURIComponent(options[key].toString());
    });

    return url;
  },

  /**
   * Get the specified page.
   * @param {!string} overlay Path relative to the html folder for the page to get.
   * @param {Object} [options] Options to append to the url.
   * @returns {!string} URL to the overlay.
   */
  getPageUrl: function (page, options) {
    return this.getResourceUrl('html/' + page, options);
  },

  /**
   * Get the specified Javascript URL; used for service workers.
   * @param {!string} overlay Path relative to the js folder for the script to get.
   * @returns {!string} URL to the script.
   */
  getScriptUrl: function (script) {
    return this.getResourceUrl('js/' + script);
  },

  /**
   * Get the specified overlay.
   * @param {!string} overlay Path relative to the overlays folder for the overlay to get.
   * @param {Object} [options] Options to append to the url.
   * @returns {!string} URL to the overlay.
   */
  getOverlayUrl: function (overlay, options) {
    return this.getPageUrl('overlays/' + overlay, options);
  },

  /**
   * Get the key management page.
   * @param {Object} [options] Options to append to the url.
   * @returns {!string} URL to the overlay.
   */
  getKeyManagerUrl: function (page, options) {
    page = page || 'index.html';
    return this.getPageUrl('key-management/' + page, options);
  },

  /**
   * Returns an object with properties and values from the current page's URL.
   *
   * @returns {*}
   */
  getUrlOptions: function () {
    if (typeof window == 'undefined') {
      throw new Errors.MGError('Cannot use URL functions; window not defined.');
    }

    var queryStartPosition = location.href.indexOf('#?');
    if (queryStartPosition == -1) {
      return {};
    }

    var query = location.href.substr(queryStartPosition + '#?'.length);

    var result = {};

    query.split('&').forEach(function (part) {
      if (!part) {
        return;
      }

      // Replace every '+' with a space.
      part = part.split('+').join(' ');

      var eqPosition = part.indexOf('=');

      if (eqPosition == -1) {
        // Will result in `key` being all of `part`, and `val` being empty.
        eqPosition = part.length;
      }

      var key = decodeURIComponent(part.substr(0, eqPosition)),
          val = decodeURIComponent(part.substr(eqPosition + 1));

      // Check whether this key is for an array.
      var from = key.indexOf('[');
      if (from == -1) {
        result[key] = val;
        return;
      }

      // Extract the actual key name, and the index name.
      var to = key.indexOf(']');
      var index = key.substring(from + 1, to);
      key = key.substring(0, from);

      // Initialize the key to point to an array.
      if (!result[key]) {
        result[key] = [];
      }

      // Necessary check, since sequential array terms are declared as "myValue[]=foobar"
      // As opposed to key-value pairs, declared as "myValue[foo]=bar"
      if (index) {
        result[key][index] = val;
      } else {
        result[key].push(val);
      }
    });

    return result;
  }
};

/**
 * Initialize base and origin.
 */
(function (urls) {
  var urlToOrigin = function (url) {
    return url.match(/^[^:]*:\/\/[^/]*/)[0];
  };

  if (urls.isExtension) {
    if (typeof kango != 'undefined') {
      urls.base = kango.io.getResourceUrl('/').replace(/\/$/, '');
    } else if (typeof location != 'undefined') {
      urls.base = urlToOrigin(location.href);
    } else {
      return;
    }

    urls.origin = urls.base;
  } else {
    urls.base = '@Server';
    urls.origin = urlToOrigin(urls.base);
  }
})(module.exports);
