var bcrypt = require('bcryptjs'),
    crypto = require('crypto'),
    util   = require('util');

var encoder = require('./encoder'),
    Errors  = require('./errors');

/**
 * Declaring constants like key sizes, algorithm names, iterations, etc.
 * All key sizes are in bits.
 */
const defaults = {
  aes:    {
    keySize:   128,
    algorithm: 'aes-128-gcm'
  },
  hash:   {
    algorithm: 'sha1'
  },
  hmac:   {
    algorithm: 'sha1'
  },
  pbkdf2: {
    keySize:    128,
    iterations: 5000 // Should be at least a hundred thousand, but performace is very bad in-browser.
  },
  bcrypt: {
    numRounds:   10,
    saltKeySize: 128
  }
};

/**
 * Ensures a value is a buffer before returning. Throws an error if anything other than
 * a string or a Buffer is passed in.
 *
 * @param {string|Buffer} value - value to wrap
 * @param {string} encoding - if provided, use this encoding instead of base64
 * @returns {Buffer}
 * @private
 */
function _toBuffer(value, encoding) {
  if (typeof value == 'string') {
    return encoder.from(value, encoding || 'base64');
  } else if (value instanceof Buffer) {
    return value;
  } else {
    throw new Errors.EncryptionError('Illegal type, must be either a string or a Buffer.');
  }
}

/**
 * Performs optional conversion from a buffer to a base64-encoded string,
 * before returning the result.
 *
 * @param {Buffer} buffer - value to be converted or passed through
 * @param {boolean} passthrough - flag to indicate whether the buffer object itself should be returned
 * @param {string} encoding - if provided, use this encoding instead of base64
 * @returns {string|Buffer}
 * @private
 */
function _toString(buffer, passthrough, encoding) {
  if (passthrough) {
    return buffer;
  } else {
    return encoder.to(buffer, encoding || 'base64');
  }
}

var Encryptor = {};

/**
 * Generates a random key of a given key size.
 *
 * @param {int} keySize - size in bits of key to return
 * @param {boolean} toBuffer - whether to pass back the raw buffer as a key
 * @returns {string|Buffer} - random key
 */
Encryptor.randomKey = function (keySize, toBuffer) {
  if (typeof keySize == 'boolean' && typeof toBuffer == 'undefined') {
    toBuffer = keySize;
    keySize = null;
  }

  keySize = keySize || defaults.aes.keySize;
  var key = crypto.randomBytes(keySize / 8);

  return _toString(key, toBuffer);
};

/**
 * Tests whether a given piece of data represents an encryption key.
 *
 * @param {string|Buffer} data - data to test for whether it is an encryption key.
 * @param {int} keySize - what size of key to test against.
 * @returns {boolean} - whether the given data represents a valid encryption key.
 */
Encryptor.isKey = function (data, keySize) {
  try {
    data = _toBuffer(data);
    keySize = keySize || defaults.aes.keySize;

    return data.length == keySize / 8;
  } catch (e) {
    return false;
  }
};

/**
 * Returns the cryptographic hash of a piece of data.
 *
 * @param {string|Buffer} data - data to hash
 * @param {boolean} toBuffer - whether to pass back the raw buffer as a hash
 * @returns {string|Buffer} - hashed value
 */
Encryptor.hash = function (data, toBuffer) {
  data = _toBuffer(data, 'utf8');
  var hash = crypto.createHash(defaults.hash.algorithm).update(data).digest();

  return _toString(hash, toBuffer);
};

/**
 * Returns the hmac of a piece of data with a given key.
 *
 * @param {string|Buffer} data - data to get hmac for
 * @param {string|Buffer} key - key for hmac
 * @param {boolean} toBuffer - whether to pass back the raw buffer as an hmac
 * @returns {string|Buffer} - hmac for data+key
 */
Encryptor.hmac = function (data, key, toBuffer) {
  data = _toBuffer(data, 'utf8');
  key = _toBuffer(key);

  var hmac = crypto.createHmac(defaults.hmac.algorithm, key).update(data).digest();

  return _toString(hmac, toBuffer);
};

/**
 * Encrypts a piece of data with a given key.
 *
 * Uses AES-128-GCM, which provides authenticity as well as confidentiality.
 *
 * @param {string|Buffer} data - data to be encrypted.
 * @param {string|Buffer} key - key to encrypt data with.
 * @returns {string|Buffer} - encrypted data
 */
Encryptor.encrypt = function (data, key) {
  data = _toBuffer(data, 'utf8');
  key = _toBuffer(key);

  var cipher = crypto.createCipher(defaults.aes.algorithm, key);

  var ciphertext = Buffer.concat([cipher.update(data), cipher.final()]);

  var encrypted = [
    encoder.toBase64(cipher.getAuthTag()), // Provides integrity.
    encoder.toBase64(ciphertext)
  ];

  return encoder.convert(JSON.stringify(encrypted), 'utf8', 'base64');
};

/**
 * Decrypts an encrypted package using a provided key.
 *
 * @param {string} data - Base64-encoded ciphertext package returned from Encryptor.encrypt
 * @param {string|Buffer} key - key to decrypt data with
 * @param {boolean} toBuffer - whether to pass back the raw buffer as decrypted data
 *                             If false, the data is converted to UTF8.
 * @returns {string|Buffer} - decrypted data
 */
Encryptor.decrypt = function (data, key, toBuffer) {
  key = _toBuffer(key);
  try {
    data = JSON.parse(encoder.convert(data, 'base64', 'utf8'));
  } catch (e) {
    throw new Errors.EncryptionError('Unable to parse encrypted data: ' + e.message);
  }

  var tag     = encoder.fromBase64(data[0]),
      content = encoder.fromBase64(data[1]);

  var decipher = crypto.createDecipher(defaults.aes.algorithm, key);

  decipher.setAuthTag(tag);

  try {
    var decrypted = Buffer.concat([decipher.update(content), decipher.final()]);
  } catch (e) {
    throw new Errors.EncryptionError('Unable to decrypt data.');
  }

  return _toString(decrypted, toBuffer, 'utf8');
};


/**
 * Performs PBKDF2 derivation on a password to produce an encryption key.
 * Also provides a bcrypt hash of the original password.
 *
 * @param {string} password - password to derive key for
 * @param {string|Buffer} salt - optional salt data to use instead of a random value.
 * @returns {string} - derived key, with salt and bcrypt hash.
 */
Encryptor.passwordToKey = function (password, salt, toBuffer) {
  if (typeof salt == 'boolean' && typeof toBuffer == 'undefined') {
    toBuffer = salt;
    salt = null;
  }

  if (!salt && typeof salt != 'string') {
    salt = Encryptor.randomKey(defaults.pbkdf2.keySize);
  }

  var key = crypto.pbkdf2Sync(password,
                              salt,
                              defaults.pbkdf2.iterations,
                              defaults.aes.keySize / 8, // AES key size so output can be used to encrypt.
                              defaults.hmac.algorithm);

  return _toString(key, toBuffer);
};

Encryptor.passwordToHash = function (password, salt) {
  var hash;

  if (salt == '') {
    var saltBuffer = new Buffer(defaults.bcrypt.saltKeySize / 8).fill(0);
    salt = util.format('$2a$%d$%s', defaults.bcrypt.numRounds,
                       bcrypt.encodeBase64(saltBuffer, defaults.bcrypt.saltKeySize / 8));
  }

  // Either use the provided salt, or generate a random one with the specified numRounds.
  if (typeof salt == 'undefined') {
    hash = bcrypt.hashSync(password, defaults.bcrypt.numRounds);
  } else {
    hash = bcrypt.hashSync(password, salt);
  }

  return hash;
};

Encryptor.passwordKeyFromPasswordAndHash = function (password, hash) {
  if (!Encryptor.verifyPasswordHash(password, hash)) {
    throw new Errors.EncryptionError('Password does not match hash.');
  }

  var salt = Encryptor.hashToSalt(hash);

  return Encryptor.passwordToKey(password, salt);
};

Encryptor.hashToSalt = function (hash) {
  var salt = bcrypt.getSalt(hash);
  return salt;
};

/**
 * Verifies that a given passwordKey matches a password key hash.
 *
 * @param {string} passwordKey - output of Encryptor.passwordToKey
 * @param {string} passwordHash - bcrypt hash
 * @returns {boolean} - whether the key and the hash match
 */
Encryptor.verifyPasswordHash = function (password, hash) {
  return bcrypt.compareSync(password, hash);
};


module.exports = Encryptor;
