/**
 * Unified error types. These functions allow us to define a heirarchy of new
 * error types, and perform robust serialization and deserialization on these types.
 */


/**
 * Initial map of all error types to create, showing their inheritance structure.
 *
 * The inheritance tree can be dynamically modified at runtime using the 'registerErrorType'
 * function. This is safe because only constructive modification is allowed.
 */
var errorTypes = {
  MGError: {
    FrontendError:          {
      CommunicatorMixinError: {}
    },
    OverlayError:           {
      PackagerError: {
        AllDecryptAttemptsFailedError: {},
        MalformedMessageError:         {},
        NoMatchingFingerprintsError:   {}
      }
    },
    KeyManagerError:        {
      KeyStorageError:        {
        KeyNotFoundError:            {},
        MasterPasswordNotFoundError: {
          MasterPasswordMissing:         {},
          MasterPasswordNeedsReentering: {}
        }
      },
      EncryptFailedError:     {
        SignFailedError:           {},
        RecipientMustInstallError: {}
      },
      DecryptFailedError:     {
        VerificationFailedError: {}
      },
      KeyCreateFailedError:   {},
      KeyInitializationError: {}
    },
    OperationTimedOutError: {},
    StorageError:           {},
    EncryptionError:        {},
    AbstractMethodError:    {}
  }
};

/**
 * @type {Object.<string, Error>} - map of error types.
 */
var mgErrorTypes = module.exports = {};

/**
 * Creates a new error type that inherits from a base error type.
 *
 * @param baseType - either a class or a class name used to look up the class in mgErrorTypes.
 * @param newTypeName - new class name
 */
module.exports.registerErrorType = function (baseType, newTypeName) {
  /*
    Sanitize newTypeName. See http://stackoverflow.com/a/19782287/3403756
   */
  newTypeName = newTypeName.replace(/^[^a-zA-Z_$]|[^\w$]/, '_');

  /*
    Look up the base type in mgErrorTypes. If 'Error' is supplied, use it directly.
   */
  if (typeof baseType == 'string') {
    baseType = mgErrorTypes[baseType];
  } else if (typeof baseType == 'function') {
    baseType = baseType === Error ? Error : mgErrorTypes[baseType.name];
  } else {
    throw new Error('Illegal invocation');
  }

  // Ensure we have a proper base type.
  if (!(baseType && (baseType === Error || baseType.prototype instanceof Error))) {
    throw new Error('Illegal base type');
  }

  // Prevent redefinition of error types.
  if (newTypeName in mgErrorTypes) {
    throw new Error('Cannot redefine error type.');
  }

  // New error constructor.
  mgErrorTypes[newTypeName] = (function () {
    /*
      This is quite hackish, but it's the only way to make the errors show up
      in the debugger with the proper name. By creating the constructor
      as a simple anonymous function, errors show up as "(anonymous function).error".
      Using Object.defineProperty will set the name, but it'll still display as an
      anonymous function. To get what we want it's either eval() or new Function().

      In ES6, this is more straightforward. See http://stackoverflow.com/a/9479081/3403756
      
      Note: Tried using an anonymous function's toString method, but uglification
      messed that up, so now the whole function body has to be in a string.
     */
    var constructorBody = ('function NAME_PLACEHOLDER(message, details) {' +
                           '  this.name = "NAME_PLACEHOLDER";' +
                           '  this.message = message;' +
                           '  this.details = details;' +
                           '}' +
                           'return NAME_PLACEHOLDER;').replace(/NAME_PLACEHOLDER/g, newTypeName),
        errConstructor  = (new Function(constructorBody))();

    // Setting up prototypical inheritance.
    errConstructor.prototype = Object.create(baseType.prototype);
    errConstructor.prototype.constructor = errConstructor;

    // Backpointer used during serialization to provide inheritance chain.
    errConstructor.inheritsFrom = baseType;

    return errConstructor;
  })();
};

/**
 * Perform initial error class creation.
 */
(function () {
  /**
   * Set up inheritance from a base name to a set of new class names.
   * Recursively calls itself on each key in the types map.
   *
   * @param {string} baseName - name of type to create inheritance from
   * @param {Object.<string, string>} typesMap - object whose keys indicate
   *                                             new types to inherit from baseName.
   */
  var makeErrorTypes = function (baseType, typesMap) {
    Object.keys(typesMap).forEach(function (newType) {
      module.exports.registerErrorType(baseType, newType);
      makeErrorTypes(newType, typesMap[newType]);
    });
  };

  /**
   * For each top-level type, make it inherit straight from Error,
   * and kick off recursive class creation down the error types tree.
   */
  makeErrorTypes(Error, errorTypes);
})();

/**
 * Assigning 'serialize' to the base class prototypes.
 * Returns a stringified JSON object containing the error information,
 * as well as an inheritance chain so the receiving end can recreate
 * the error's inheritance properly if it does not already exist on that end.
 */
Object.keys(errorTypes).forEach(function (topLevelType) {
  mgErrorTypes[topLevelType].prototype.serialize = function () {
    return JSON.stringify({
                            name:             this.name,
                            message:          this.message,
                            details:          this.details,
                            inheritanceChain: (function (ancestorType) {
                              var chain = [];

                              /*
                                There was no reliable way to use built-in reflection
                                to get the next constructor up in the inheritance chain,
                                so we explicitly set 'inheritsFrom' during class creation
                                and can use it here.

                                Eventually the chain will start with 'MGError' and end with
                                the class name from which this error directly inherits.
                               */
                              while (ancestorType && ancestorType !== Error) {
                                chain.unshift(ancestorType.name);
                                ancestorType = ancestorType.inheritsFrom;
                              }

                              return chain;
                            })(this.constructor.inheritsFrom)
                          });
  };
});

/**
 * Restores an error from a stringified representation.
 *
 * If necessary, reconstructs the inheritance chain. This is used if
 * one running instance has defined a custom error type that this instance
 * has not defined. In order to preserve inheritance data, the chain
 * of class names is used to recreate the classes on this side.
 *
 * @param {string} error - stringified representation of error.
 * @returns {Error} - reconstructed MGError instance.
 */
module.exports.parse = function (error) {
  var errProps = JSON.parse(error);

  if (!errProps.name || !errProps.inheritanceChain) {
    throw new Error('Cannot parse error.');
  }

  if (!(errProps.name in mgErrorTypes)) {
    // We don't already have an error of this type, so we need to follow the
    // inheritance chain to recreate it.
    var chain = errProps.inheritanceChain;

    try {
      for (var i = 0; i < chain.length; i++) {
        // Continue until we're at one we haven't seen before.
        if (chain[i] in mgErrorTypes) {
          continue;
        }

        var prevType = i == 0 ? Error : chain[i - 1];
        var nextType = chain[i];

        module.exports.registerErrorType(prevType, nextType);
      }

      // The chain is created, now we can recreate the main type.
      var baseType = chain.length ? chain[chain.length - 1] : Error;
      module.exports.registerErrorType(baseType, errProps.name);

    } catch (e) {
      throw new Error('Cannot parse error type: ' + errProps.name + ' (' + e.toString() + ')');
    }
  }

  // Ensure we have a proper error type now.
  if (!(mgErrorTypes[errProps.name].prototype instanceof Error)) {
    throw new Error('Cannot instantiate non-error type.');
  }

  return new mgErrorTypes[errProps.name](errProps.message, errProps.details);
};
