/**
 * Mixin for allowing windows to communicate using message channels.
 * @module
 */
"use strict";

var MessageType = {
  UNLISTED:                "unlisted",
  OVERLAY_READY:           "overlay_ready",
  MESSAGE_ENCRYPTED:       "message_encrypted",
  CANCEL:                  "cancel",
  CHANGE_BACKGROUND_COLOR: "change_background_color",
  DISCARD_DRAFT:           "discard_draft",
  SAVE_DRAFT:              "save_draft",
  COMPOSE_INSTALL_PROMPT:  "compose_install_prompt",
  GET_RECIPIENTS:          'get_recipients',
  GET_CONTENTS:            'get_contents',
  SEND_CONTENTS:           'send_contents',
  FOCUS_CHANGED:           'focus_changed',
  SIZING_INFO:             'sizing_info',
  TUTORIAL_MASK_PLACE:     'tutorial_mask_place',
  TUTORIAL_MASK_REMOVE:    'tutorial_mask_remove'
};

module.exports = MessageType;
