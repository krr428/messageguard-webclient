/**
 * Mixin for allowing windows to communicate using message channels.
 * @module
 */
"use strict";

var EventEmitter = require('events');

/*var WindowMessageType = {
  UNLISTED: 1,
  RANDOM: 2
};*/

/**
 * Mixin for providing communication between windows.
 * @constructor
 * @alias module:common/communicator-mixin
 */
var communicatorMixin = function () {

  /**
   *
   */
  this.messagePort = null;

  /**
   *
   */
  this.eventEmitter = new EventEmitter();

  this.postMessage = function (messageType, message) {

    var typedMessage = {
      type: messageType,
      data: message
    };

    if (this.messagePort) {
      this.messagePort.postMessage(typedMessage);
    } else {
    }
  };

  this.handleMessage = function (event) {
    var type = event.data.type;
    var data = event.data.data;
    this.eventEmitter.emit(type, data);
  };

  this.registerMessageHandler = function (messageType, once, handler) {
    if (once) {
      this.eventEmitter.once(messageType, handler.bind(this));
    }
    else {
      this.eventEmitter.on(messageType, handler.bind(this));
    }
  };

  this.removeMessageHandler = function (messageType, handler) {
    this.eventEmitter.removeListener(messageType, handler);
  };
};

module.exports = communicatorMixin;
