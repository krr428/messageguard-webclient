/**
 * Converts data from one representation to another.
 *
 * @param data - string data to convert
 * @param from - from encoding
 * @param to - to encoding
 * @returns {string} - encoded data
 */
function convert(data, from, to) {
  return new Buffer(data, from).toString(to);
}

// Convenience methods
convert.to = function (data, to) {
  return data.toString(to);
};

convert.from = function (data, from) {
  return new Buffer(data, from);
};

module.exports = {
  convert: convert,

  to:   convert.to,
  from: convert.from,

  toHex:   function (data) { return convert.to(data, 'hex'); },
  fromHex: function (data) { return convert.from(data, 'hex'); },

  toUTF8:   function (data) { return convert.to(data, 'utf8'); },
  fromUTF8: function (data) { return convert.from(data, 'utf8'); },

  toBase64:   function (data) { return convert.to(data, 'base64'); },
  fromBase64: function (data) { return convert.from(data, 'base64'); },
};
