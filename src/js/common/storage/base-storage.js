var Errors = require('../errors'),
    format = require('util').format;

function BaseStorage(storageType) {
  this.storageType = storageType;
}

BaseStorage.prototype._getItem = function () {
  throw new Errors.AbstractMethodError('Not implemented');
}

BaseStorage.prototype._setItem = function () {
  throw new Errors.AbstractMethodError('Not implemented');
}

BaseStorage.prototype._removeItem = function () {
  throw new Errors.AbstractMethodError('Not implemented');
}

BaseStorage.prototype.getItem = function (key) {
  key = this._getStorageKey(key);
  return this._getItem(key);
}

BaseStorage.prototype.setItem = function (key, value) {
  key = this._getStorageKey(key);
  return this._setItem(key, value);
}

BaseStorage.prototype.removeItem = function (key) {
  key = this._getStorageKey(key);
  return this._removeItem(key);
}

BaseStorage.prototype.getKeys = function () {
  var self = this;
  return this._getRawKeys().then(function (keys) {
    return keys.filter(self._isStorageKey.bind(self))
        .map(self._getOriginalKey.bind(self));
  });
};

BaseStorage.prototype._getStorageKey = function (key) {
  return format('%s:%s', this.storageType, key);
};

BaseStorage.prototype._isStorageKey = function (key) {
  return new RegExp(format('^%s:', this.storageType)).test(key);
};

BaseStorage.prototype._getOriginalKey = function (key) {
  return key.replace(new RegExp(format('^%s:', this.storageType)), '');
};

BaseStorage.prototype.clear = function () {
  var self = this;
  return this.getKeys().then(function (keys) {
    return Promise.all(keys.map(self.removeItem.bind(self)));
  });
}

BaseStorage.prototype._getStorage = function () {
  throw new Errors.AbstractMethodError('Not implemented');
}

BaseStorage.prototype.initialize = function () {
  if (this._isInitialized) {
    return Promise.resolve();
  } else {
    this._isInitialized = true;
    return this._initialize();
  }
}

module.exports = BaseStorage;