var isWorker = typeof window === 'undefined';

var storageObjects = isWorker ? require('./worker-objects') : require('./native-objects');

module.exports = {
  local:        storageObjects.local,
  session:      storageObjects.session,
  cookies:      storageObjects.cookies,
  eventEmitter: storageObjects.eventEmitter
};
