var EventEmitter = require('./native-storage-event-emitter'),
    Errors       = require('../../errors');

var isWorker = typeof window === 'undefined';

if (isWorker) {
  throw new Errors.StorageError('Cannot use native storage objects in a worker thread.');
}

function NativeStorage(storageObject) {
  this.storageObject = storageObject;
}

NativeStorage.prototype.getItem = function (key) {
  return Promise.resolve(this.storageObject.getItem(key));
};

NativeStorage.prototype.setItem = function (key, value) {
  return Promise.resolve(this.storageObject.setItem(key, value));
};

NativeStorage.prototype.removeItem = function (key) {
  return Promise.resolve(this.storageObject.removeItem(key));
};

NativeStorage.prototype.getKeys = function () {
  return Promise.resolve(Object.keys(this.storageObject));
};

NativeStorage.prototype.clear = function () {
  return Promise.resolve(this.storageObject.clear());
};


function NativeCookies() {}

NativeCookies.prototype.getCookie = function (cname) {
  var name = cname + '=';
  var ca = document.cookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return Promise.resolve(c.substring(name.length, c.length));
    }
  }

  return Promise.resolve('');
};

NativeCookies.prototype.setCookie = function (name, value, path, expiration) {
  var cookieString = name + '=' + value + ';';

  if (path) {
    cookieString += ' path=' + path + ';';
  }

  if (expiration) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    cookieString += ' expires=' + d.toUTCString() + ';';
  }

  document.cookie = cookieString;
  return Promise.resolve();
};


function NativeStorageEvents() {
  this.eventEmitter = new EventEmitter();
}

NativeStorageEvents.prototype.on = function (eventName, cb) {
  this.eventEmitter.on(eventName, cb);
  return Promise.resolve();
};

NativeStorageEvents.prototype.off = function (eventName, cb) {
  this.eventEmitter.off(eventName, cb);
  return Promise.resolve();
};

NativeStorageEvents.prototype.once = function (eventName, cb) {
  this.eventEmitter.once(eventName, cb);
  return Promise.resolve();
};

NativeStorageEvents.prototype.emit = function (eventName, data) {
  this.eventEmitter.emit(eventName, data);
  return Promise.resolve();
};

var storageObjects = {
  local:        new NativeStorage(localStorage),
  session:      new NativeStorage(sessionStorage),
  cookies:      new NativeCookies(),
  eventEmitter: new NativeStorageEvents()
};

function handleWorkerRequest(request, responseCB) {
  switch (request.storageType) {
    case 'local':
    case 'session':
    case 'cookies':
      var storageObject = storageObjects[request.storageType];
      var storageMethod = storageObject[request.methodName];
      if (typeof storageMethod != 'function') {
        throw new Errors.StorageError('Illegal request storage method: ' + request.methodName);
      }

      return storageMethod.apply(storageObject, request.args);
    case 'eventEmitter':
      return handleStorageEventRequest(request, responseCB);
    default:
      throw new Errors.StorageError('Illegal request storage type: ' + request.storageType);
  }
};

var handleStorageEventRequest = (function () {
  var eventEmitter = storageObjects.eventEmitter;

  var listeners = {};

  var makeListener = function (callbackUUID, once, responseCB) {
    if (!(callbackUUID in listeners)) {
      listeners[callbackUUID] = function (event) {

        responseCB({
                     callbackUUID: callbackUUID,
                     eventData:    event
                   });

        if (once) {
          delete listeners[callbackUUID];
        }
      };
    }

    return listeners[callbackUUID];
  };

  return function (request, responseCB) {
    switch (request.methodName) {
      case 'emit':
        break; // No transformation necessary.
      case 'on':
      case 'once':
        var once = request.methodName == 'once';
        request.args[1] = makeListener(request.args[1], once, responseCB);
        break;
      case 'off':
        var listenerFunc = this.listeners[request.args[1]];
        if (listenerFunc) {
          delete this.listeners[request.args[1]];
        }
        request.args[1] = listenerFunc;
        break;
      default:
        throw new Errors.StorageError('Illegal storage event action type: ' + task.actionType);
    }

    return eventEmitter[request.methodName].apply(eventEmitter, request.args);
  };
})();

module.exports = {
  local:               storageObjects.local,
  session:             storageObjects.session,
  cookies:             storageObjects.cookies,
  eventEmitter:        storageObjects.eventEmitter,
  handleWorkerRequest: handleWorkerRequest
};
