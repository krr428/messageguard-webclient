/**
 * Generic read class.
 * @module
 * @requires bower_components/tether
 * @requires common/jquery-bootstrap
 * @requires overlays/overlay-base
 * @requires common/message-type
 */
'use strict';

var $           = require('../common/jquery-bootstrap'),
    OverlayBase = require('./overlay-base'),
    MessageType = require('../common/message-type'),
    sanitizer   = require('../common/html-sanitizer'),
    Errors      = require('../common/errors');

/**
 * Controls the generic read interface
 * @constructor
 * @alias module:overlays/generic-read
 */
function GenericRead() {
}

GenericRead.prototype = new OverlayBase();
GenericRead.prototype.constructor = GenericRead;

/**
 * Initialize and begin operating
 */
GenericRead.prototype.init = function () {
  OverlayBase.prototype.init.call(this);

  this.uuid = this.getQueryStringParam('uuid');
  this.updateView();
  this.bindUIActions();

  //REGISTER MESSAGE HANDLERS

  // Add tooltips
  $('[title]').tooltip({
                         container: 'body'
                       }).on('click', function () {
    $(this).tooltip('hide');
  });

  this.registerMessageHandler(MessageType.SEND_CONTENTS, false, this.sendContentsHandler);
};

GenericRead.prototype.sendContentsHandler = function (data) {
  var self = this;
  self.keyManagerReady.then(function () {
    return self.performDecryption(data);
  }).then(function (decryptedData) {
    self.showMaskedMessage(data.package, {
      fadeOut:        true,
      fadeSpeed:      1000,
      contentWrapper: '<span style="display: block; padding: 50px 10px 10px 10px; line-height: 1.5; font-size: 14px; word-wrap: break-word;" />'
    });
    self.setDisplayContent(decryptedData);

    window.setTimeout(function() {
      self.hideMaskedMessage();
      self.postMessage(MessageType.SIZING_INFO, self.getHeight());
    }, 1000);
  }).catch(function (error) {
    var errorMessage;
    switch (error.name) {
      case Errors.MalformedMessageError.name:
        self.showMaskedMessage(
            'This message is malfored. Please contact the sencder and ask them to resend it.',
            {isError: true});
        break;

      case Errors.AllDecryptAttemptsFailedError.name:
        self.showMaskedMessage("You don't have permission to decrypt this message.", {isError: true});
        break;
      default:
        self.showMaskedMessage(error, {isError: true});
    }
  }).then(function () {
    self.postMessage(MessageType.SIZING_INFO, self.getHeight());
  });
};

GenericRead.prototype.getHeight = function () {
  var smallHeight   = $('#small:visible').height() || 0,
      largeHeight   = $('#large:visible').height() || 0,
      overlayHeight = $('#message-mask:visible > span').height() + 20 || 0;

  return Math.max(smallHeight, largeHeight, overlayHeight);
};

/**
 * Signals that the key info has been updated
 */
GenericRead.prototype.keysUpdated = function () {
  //Does nothing for now
};

GenericRead.prototype.setKeyIconInfo = function (keyAttributes, fromID) {
  var keyLabelText = fromID ? 'Verified sender: ' + fromID.value : '';

  $('.key-icon').css('color', keyAttributes.color);
  $('.key-icon').css('text-shadow', '1px 1px 1px ' + keyAttributes.color);
  $('.key-btn').attr('data-original-title', keyLabelText);
  $('.key-btn #key-text').text(keyAttributes.name);
};

GenericRead.prototype.performDecryption = function (encryptedData) {
  this.showMaskedMessage('Decrypting message &hellip;', {showSpinner: true});

  var self = this;
  return this.packager.decrypt(encryptedData.package).then(function (result) {
    self.setKeyIconInfo(result.keyAttributes, result.fromID);
    var sanitizedData = sanitizer.sanitize(result.data);
    self.hideMaskedMessage();
    return sanitizedData;
  }).catch(function (error) {
    if (!(error instanceof Errors.NoMatchingFingerprintsError)) {
      throw error;
    }

    var result = self.getNoKeyAvailableMessage({
                                                 scheme:        error.details.scheme,
                                                 fingerprints:  error.details.messageFingerprints,
                                                 forEncryption: false
                                               });

    self.showMaskedMessage(result.messageElement, {isError: true});
    return result.promise.then(function () {
      return self.performDecryption(encryptedData);
    });
  });
};

/**
 * Update the content display. Goes full width for small display
 * and goes full width and height(besides top bar) for large display
 */
GenericRead.prototype.updateView = function () {
  if ($("#small").css('display') != 'none') {

    this.display = $("#small-display");

    var height = $("body").height() + 20;
    this.display.css('min-height', height + 'px');
    this.display.css('max-height', height + 'px');

    var topPadding = ($('body').height() - 19) / 2;
    this.display.css('padding-top', topPadding + 'px');
  }
};

GenericRead.prototype.setDisplayContent = function (content) {
  $("#large-display").html(content);
  $("#small-display").html(content);
};

/**
 * Binds updateView to window resize UI action
 */
GenericRead.prototype.bindUIActions = function () {
  $(window).resize(this.updateView);
};

module.exports = GenericRead;
