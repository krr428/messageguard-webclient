/**
 * Generic read class.
 * @module
 * @requires bower_components/tether
 * @requires common/jquery-bootstrap
 * @requires overlays/overlay-base
 * @requires common/message-type
 */
'use strict';

window.Tether = require('tether');

var $           = require('../../../common/jquery-bootstrap'),
    GenericRead = require('../../generic-read'),
    MessageType = require('../../../common/message-type'),
    sanitizer   = require('../../../common/html-sanitizer');

/**
 * Controls the generic read interface
 * @constructor
 * @alias module:overlays/generic-read
 */
function GmailRead() {

}

GmailRead.prototype = new GenericRead();
GmailRead.prototype.constructor = GmailRead;

/**
 * Initialize and begin operating
 */
GmailRead.prototype.init = function () {
  var self = this;

  GenericRead.prototype.init.call(this);

  this.registerMessageHandler(MessageType.SIZING_INFO, false, function () {
    self.postMessage(MessageType.SIZING_INFO, self.getHeight());
  });

  this.uuid = this.getQueryStringParam('uuid');
  //this.updateView();
  this.bindUIActions();

  //REGISTER MESSAGE HANDLERS

  // Add tooltips
  $('[title]').tooltip({
                         container: 'body'
                       }).on('click', function () {
    $(this).tooltip('hide');
  });

  this.registerMessageHandler(MessageType.SEND_CONTENTS, false, this.sendContentsHandler);

  this.registerMessageHandler(MessageType.TUTORIAL_MASK_PLACE, false, function () {
    self.placeKeyButtonTutorialMask();
  });

  this.registerMessageHandler(MessageType.TUTORIAL_MASK_REMOVE, false, function () {
    self.removeKeyButtonTutorialMask();
  });
};

GmailRead.prototype._showOverlay = function () {
  GenericRead.prototype._showOverlay.apply(this, arguments);
  this.postMessage(MessageType.SIZING_INFO, this.getHeight());
};

GmailRead.prototype.placeKeyButtonTutorialMask = function () {
  if ($('#large').is(':visible')) {

    $('.key-btn').filter(':visible')
        .addClass('tutorial-focus')
        .after('<div class="tutorial-mask"></div>');

  } else if ($('#small').is(':visible')) {

    $('.key-btn').filter(':visible').parent().parent().addClass('tutorial-focus');
    $('#large, #small').filter(':visible').append('<div class="tutorial-mask"></div>');

  }
};

GmailRead.prototype.removeKeyButtonTutorialMask = function () {
  $('.tutorial-focus').removeClass('tutorial-focus');
  $('.tutorial-mask').remove();
};

/**
 * Binds updateView to window resize UI action
 */
GmailRead.prototype.bindUIActions = function () {
  //$(window).resize(this.updateView);
};

var GmailRead = new GmailRead();
GmailRead.init();
