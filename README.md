# MessageGuard Web Client

This is the web client for MessageGuard. It works with most desktop browsers using an extension (Chrome, Firefox, Safari, Opera; coming soon: Edge). A bookmarklet is used for Internet Explorer and mobile browsers.

### What is this repository for? ###

This repository is maintained by the Internet Security Research Lab. We are open to collaboration with anyone that would like to increase the functionality of Message Guard. Feel free to fork the repository, and submit pull requests with finished code. Ensure that your code passes the linter, and that it is fully documented for use with JSDoc.

### How do I get set up? ###

1. Install Node JS, Python, and git. Ensure that they are in your path.

2. Run the setup script. This will install gulp and bower globally.
```
#!bash
./setup.sh
```

or 

```
#!bash
setup.bat
```

### Building the files. ###

You can build message guard as a browser extension or as a bookmarklet. The bookmarklet is really just a bunch of files that go on a webserver with a bookmarklet that loads the correct files.

Use gulp to build:

| Command | Functionality |
| --- | --- |
| gulp server | Builds everything and pushes files to the server. |
| gulp server:files | Builds the server files but don't push them to the server. Files are in build/server. |
| gulp extension | Builds everything and compiles the extension for Chrome. |
| gulp extension:files | Builds the extension files but don't compile the extension. Files are in build/extension. |
| gulp js:lint | Lints the JavaScript source. |

The available flags are:

| Flag | Functionality |
| --- | --- |
| -w --watch | Watch for changes in the underlying files and automatically re-run the gulp task.
| -d --debug | Make the output more acceptable for debugging. Right now this is always true. |


### Contribution guidelines ###
Make sure all of your files lint and follow the style defined in the idea style file.

### Who do I talk to? ###
For questions or comments contact Scott Ruoti <ruoti@isrl.byu.edu>.