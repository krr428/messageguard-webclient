﻿function openInitWindow() {
  window.open(kango.io.getResourceUrl('res/html/key-management/initialize.html'));
}

kango.ui.browserButton.addEventListener(kango.ui.browserButton.event.COMMAND, function() {
  openInitWindow();
  //kango.ui.optionsPage.open();
});

var hasDoneInitialSetup = kango.storage.getItem('initial_setup_done');
if (!hasDoneInitialSetup) {
  // Remember that we've performed setup.
  kango.storage.setItem('initial_setup_done', true);
  openInitWindow();
}

kango.addMessageListener('initialization_complete', function () {
  // Only do this the first time.
  if (kango.storage.getItem('init_window_messaged')) {
    return;
  }

  kango.storage.setItem('init_window_messaged', true);
  refreshGmailTabs();
});

function refreshGmailTabs() {
  kango.browser.tabs.getAll(function (tabs) {
    tabs.filter(function (tab) {
      return /^https?:\/\/mail.google.com(\/|:|$)/i.test(tab.getUrl());
    }).forEach(function (tab) {
      tab.navigate(tab.getUrl());
    });
  });
}

var xhr = new XMLHttpRequest();
xhr.open("GET", kango.io.getResourceUrl('res/js/extension.js'), false);
xhr.send(null);
var extensionScript = xhr.responseText;

kango.addMessageListener('GetExtensionScript', function(event) {
  event.source.dispatchMessage(event.data, extensionScript);
});
