var Cc = Components.classes;
var Ci = Components.interfaces;
var Cu = Components.utils;
var Cm = Components.manager;
var Cr = Components.results;

Cu['import']('resource://gre/modules/Services.jsm');
Cu['import']('resource://gre/modules/AddonManager.jsm');
Cu['import']('resource://gre/modules/FileUtils.jsm');
Cu['import']("resource://gre/modules/XPCOMUtils.jsm");
Cu['import']("resource://gre/modules/devtools/Console.jsm");

function log(str) {
  Services.console.logStringMessage(str)
}

var loader = null;

function getExtensionInfo(data) {
  var req = Cc['@mozilla.org/xmlextras/xmlhttprequest;1'].createInstance(Ci.nsIXMLHttpRequest);
  req.open('GET', data.resourceURI.spec + 'extension_info.json', false);
  req.overrideMimeType('text/plain');
  req.send(null);
  return JSON.parse(req.responseText);
}

function Module(id, require, props) {
  this.XMLHttpRequest = function () {
    return Cc['@mozilla.org/xmlextras/xmlhttprequest;1'].createInstance(Ci.nsIXMLHttpRequest);
  };

  this.alert = function (str) {
    Services.prompt.alert(null, 'Kango', str);
  };

  this.log = log;
  this.id = id;
  this.exports = {};
  this.require = require;
  this.module = this;

  this.Services = Services;
  this.FileUtils = FileUtils;

  this.Cc = Cc;
  this.Ci = Ci;
  this.Cu = Cu;
  this.Cm = Cm;
  this.Cr = Cr;

  if (props) {
    for (var key in props) {
      if (props.hasOwnProperty(key)) {
        this[key] = props[key];
      }
    }
  }
}

function Loader(resolvePath, props, overrides) {

  var modules = {};

  function require(id) {
    if (overrides && overrides.hasOwnProperty(id)) {
      return overrides[id];
    }
    if (!modules[id]) {
      var principal = Cc['@mozilla.org/systemprincipal;1'].getService(Ci.nsIPrincipal);
      var module = modules[id] = new Cu.Sandbox(principal, {
        sandboxName:      id,
        sandboxPrototype: new Module(id, require, props),
        wantComponents:   false,
        wantXrays:        false
      });
      var path = resolvePath(id);
      if (path) {
        Services.scriptloader.loadSubScript(path, module, 'UTF-8');
      }
      else {
        throw new Error('Unable to find module with id=' + id)
      }
    }
    return modules[id].exports;
  }

  function dispose() {
    for (var key in modules) {
      if (modules.hasOwnProperty(key)) {
        var module = modules[key];
        if (module.exports.dispose) {
          module.exports.dispose();
        }
        if (module.dispose) {
          module.dispose();
        }
      }
    }
    for (var key in modules) {
      if (modules.hasOwnProperty(key)) {
        var module = modules[key];
        for (var k in module) {
          module[k] = null;
        }
        modules[key] = null;
      }
    }
    modules = {};
  }

  return {
    require: require,
    dispose: dispose
  };
}

function loadServices(loader, info) {
  var modules = [
    'kango/userscript_engine',
    'kango/backgroundscript_engine',
    'kango/api'
  ];

  if (info.modules) {
    modules = modules.concat(info.modules);
  }

  for (var i = 0; i < modules.length; i++) {
    loader.require(modules[i]);
  }
}

function init(startupData) {
  AddonManager.getAddonByID(startupData.id, function (addon) {
    var info = getExtensionInfo(startupData);
    var resolvePath = function (id) {
      var filename = id + '.js';
      if (addon.hasResource(filename)) {
        return addon.getResourceURI(filename).spec;
      }
      return null;
    };
    loader = new Loader(resolvePath, {
      __extensionInfo: info,
      __installPath:   startupData.installPath
    });
    loadServices(loader, info);
    loader.require('kango/core').init();
  });
}

// bootstrap.js required exports

function install(data, reason) {
  // Changes to allow channel messaging.
  Components.classes["@mozilla.org/preferences-service;1"]
      .getService(Components.interfaces.nsIPrefService)
      .getBranch("dom.messageChannel.")
      .setBoolPref("enabled", true);
// End custom changes.
}

function uninstall(data, reason) {
  if (reason == ADDON_UNINSTALL) {

    // Changes to allow channel messaging.
    Components.classes["@mozilla.org/preferences-service;1"]
        .getService(Components.interfaces.nsIPrefService)
        .getBranch("dom.messageChannel.")
        .resetBranch();
    // End custom changes.

    var resolvePath = function (id) {
      return data.resourceURI.spec + id + '.js';
    };
    var info = getExtensionInfo(data);
    var loader = new Loader(resolvePath, null, {
      'kango/core':           {
        addAsyncModule: function () {},
        fireEvent:      function () {},
        uninstall:      true
      },
      'kango/extension_info': info
    });
    loader.require('kango/uninstall')();
  }
}

// Changes to allow resource URLS to be loaded into iframes.
// See: http://cat-in-136.github.io/2014/11/How-to-create-a-javascript-xpcom-component-on-bootstrapped-addon.html
var resourceProtocolHandler = Cc["@mozilla.org/network/io-service;1"]
    .getService(Ci.nsIIOService)
    .getProtocolHandler('resource');

function ResourceProtocol() {
}
ResourceProtocol.ExtensionId = 'kango-c0983b9e-af1a-4598-8c5f-69eba1cb277e';
ResourceProtocol.Scheme = 'res-' + ResourceProtocol.ExtensionId;
ResourceProtocol.Prefix = ResourceProtocol.Scheme + "://"  + ResourceProtocol.ExtensionId + "/"
ResourceProtocol.PrefixLength = ResourceProtocol.Prefix.length;

ResourceProtocol.prototype = {
  defaultPort:   -1,
  protocolFlags: Ci.nsIProtocolHandler.URI_STD
                 | Ci.nsIProtocolHandler.URI_LOADABLE_BY_ANYONE
                 | Ci.nsIProtocolHandler.URI_SAFE_TO_LOAD_IN_SECURE_CONTEXT,

  newURI: function (spec, originCharset, baseURI) {
    let uri = Cc["@mozilla.org/network/standard-url;1"].createInstance(Ci.nsIStandardURL);
    uri.init(uri.URLTYPE_STANDARD, this.defaultPort, spec, originCharset, baseURI);
    return uri.QueryInterface(Ci.nsIURI);
  },

  newChannel: function (uri) {
    // Ensure the URI has the correct form.
    if (uri.spec.indexOf(ResourceProtocol.Prefix) != 0 || !(
        uri.spec.indexOf('res/') == ResourceProtocol.PrefixLength ||
        uri.spec.indexOf('assets/') == ResourceProtocol.PrefixLength ||
        uri.spec.indexOf('icons/') == ResourceProtocol.PrefixLength
        )) {
      throw Cr.NS_ERROR_ILLEGAL_VALUE;
    }
    var resourceUri = resourceProtocolHandler.newURI(uri.spec.replace(ResourceProtocol.Scheme, "resource"),
                                                     uri.originCharset, null);
    var channel = resourceProtocolHandler.newChannel(resourceUri);
    channel.originalURI = uri;
    return channel;
  },

  allowPort: function (port, scheme) {
    return false;
  },

  classDescription: "Resource iFrame Protocol Handler",
  contractID:       "@mozilla.org/network/protocol;1?name=" + ResourceProtocol.Scheme,
  classID:          Components.ID('{90900150-de64-11e4-8830-0800200c9a66}'), // Randomly generated.
  QueryInterface:   XPCOMUtils.generateQI([Ci.nsIProtocolHandler])
}

const ResourceProtocolFactory = Object.freeze({
  createInstance: function (aOuter, aIID) {
    if (aOuter) { throw Cr.NS_ERROR_NO_AGGREGATION; }
    return new ResourceProtocol();
  },
  loadFactory:    function (aLock) { /* unused */ },
  QueryInterface: XPCOMUtils.generateQI([Ci.nsIFactory])
});

// Circumvent CSP protections for these iFrames.
function _addResourceDomain(cspRules) {
  var rules            = cspRules.split(';'),
      protocolToAdd    =  ' ' + ResourceProtocol.Scheme + ':',
      frameSrcDefined  = false,
      scriptSrcDefined = false,
      defaultSrcIndex  = -1;

  for (var ii = 0; ii < rules.length; ii++) {
    if (rules[ii].toLowerCase().indexOf('script-src') != -1) {
      rules[ii] = rules[ii] + protocolToAdd;
      scriptSrcDefined = true;
    }

    if (rules[ii].toLowerCase().indexOf('frame-src') != -1) {
      rules[ii] = rules[ii] + protocolToAdd;
      frameSrcDefined = true;
    }

    if (rules[ii].toLowerCase().indexOf('default-src') != -1) {
      defaultSrcIndex = ii;
    }
  }

  // Some websites will put every thing in the default (default-src) directive,
  // without defining script-src. We need to modify those as well.
  if ((!scriptSrcDefined || !frameSrcDefined) && (defaultSrcIndex != -1)) {
    rules[defaultSrcIndex] = rules[defaultSrcIndex] + protocolToAdd;
  }

  return rules.join(';');
};

function _httpExamineCallback(aSubject, aTopic, aData) {
  var httpChannel = aSubject.QueryInterface(Ci.nsIHttpChannel);

  if (httpChannel.responseStatus !== 200) {
    return;
  }

  var cspRules;
  var mycsp;
  // There is no clean way to check the presence of csp header. An exception will be thrown if it is not there.
  // https://developer.mozilla.org/en-US/docs/XPCOM_Interface_Reference/nsIHttpChannel

  try {
    cspRules = httpChannel.getResponseHeader("Content-Security-Policy");
    mycsp = _addResourceDomain(cspRules);
    httpChannel.setResponseHeader('Content-Security-Policy', mycsp, false);
  } catch (e) {
    try {
      // Fallback mechanism support.
      cspRules = httpChannel.getResponseHeader("X-Content-Security-Policy");
      mycsp = _addResourceDomain(cspRules);
      httpChannel.setResponseHeader('X-Content-Security-Policy', mycsp, false);
    } catch (e) {
      // No csp headers defined.
      return;
    }
  }
};
// End custom changes.

function startup(startupData, reason) {
  // Modified code for iFrame display
  var registrar = Components.manager.QueryInterface(Ci.nsIComponentRegistrar);
  registrar.registerFactory(ResourceProtocol.prototype.classID,
                            ResourceProtocol.prototype.classDescription,
                            ResourceProtocol.prototype.contractID,
                            ResourceProtocolFactory);
  Cc["@mozilla.org/observer-service;1"].getService(Ci.nsIObserverService)
      .addObserver(_httpExamineCallback, "http-on-examine-response", false);
  // End modified code.

  var hiddenDOMWindow;
  try {
    hiddenDOMWindow = (Services.appShell ||
                       Cc['@mozilla.org/appshell/appShellService;1'].getService(Ci.nsIAppShellService)).hiddenDOMWindow;
  }
  catch (e) {
  }
  if (hiddenDOMWindow) {
    init(startupData);
  } else {
    var onFinalUiStartup = function (subject, topic, data) {
      Services.obs.removeObserver(onFinalUiStartup, 'final-ui-startup', false);
      init(startupData);
    };
    Services.obs.addObserver(onFinalUiStartup, 'final-ui-startup', false);
  }
}

function shutdown(data, reason) {
  // Modified code for iFrame display
  var registrar = Components.manager.QueryInterface(Ci.nsIComponentRegistrar);
  registrar.unregisterFactory(ResourceProtocol.prototype.classID, ResourceProtocolFactory);
  Cc["@mozilla.org/observer-service;1"].getService(Ci.nsIObserverService)
      .removeObserver(_httpExamineCallback, "http-on-examine-response");
  // End modified code.


  if (reason != APP_SHUTDOWN) {
    if (loader) {
      loader.dispose();
      loader = null;
    }
  }
}